#### Compiler 6

Added code generator which uses a 3 stages to convert the parser's Abstract Syntax Tree into TM code.

Stage1: Linearize the AST by converting it into an intermediate language.

Stage2: Convert stage1's intermediate language into TM code.

Stage3: Format and output stage2's TM code into a valid TM program file.

Also added `twinec` script to serve as the code generator's front-end.

New twine program 'sum-square-diff.twn' included in programs directory.
