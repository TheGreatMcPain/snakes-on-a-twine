## For developers

### Useful git commands

The following git commands must be run while inside the repository.

You can add the `--global` flag to set these globally, but by default they only affect whatever git repository you're currently in.

#### Set user and email

```
git config user.name "<insert your name>"
git config user.email "<insert your email>"
```

#### Save username and password

WARNING: Your password will be saved in plain text in a file called `.git-credentials`.

```
git config credential.helper store
```

There's a more secure method, but it requires setting up GnuPG.
