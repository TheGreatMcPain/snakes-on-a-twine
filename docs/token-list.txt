Tokens:

1. Identifier

Positive examples:

	main
	print
	Main
	Print
	Identifier
	myIdentifier
	$identifier
	My_Identifier
	identifier1
	ident_123$_ifier3
	i
	aaa ... a (256 times)

Negative examples:

	123identifier
	identifier*
	my-identifier
	(this%#identifier)
	aaa ... a (257 times)
	integer
	_identifier
	returns


2. Keyword

Positive examples:

	if
	else
	f
	returns

Negative examples:

	If
	Else
	notAKeyWord
	keyword123


3. Integer

Positive examples:

	0
	1
	123456
	1025102
	2^31 - 1 (if it were written as a normal integer)

Negative examples:

	0123
	-123
	+123
	abc
	123abc
	3.14
	1e02
	2^31


4. Boolean

Positive examples:

	true
	false

Negative examples:

	Anything other than the above


5. Operator equals

Positive examples:

	=

Negative examples:

	Anything other than the above


6. Operator plus

Positive examples:

	+

Negative examples:

	Anything other than the above


7. Operator minus

Positive examples:

	-

Negative examples:

	Anything other than the above


8. Operator multiply

Positive examples:

	*

Negative examples:

	Anything other than the above


9. Operator divide

Positive examples:

	/

Negative examples:

	Anything other than the above


10. Operator lessthan

Positive examples:

	<

Negative examples:

	Anything other than the above


11. Operator and

Positive examples:

	^

Negative examples:

	Anything other than the above


12. Operator or

Positive examples:

	|

Negative examples:

	Anything other than the above


13. Operator not

Positive examples:

	~

Negative examples:

	Anything other than the above


14. Punct leftparen

Positive examples:

	(

Negative examples:

	Anything other than the above


15. Punct rightparen

Positive examples:

	)

Negative examples:

	Anything other than the above


16. Punct colon

Positive examples:

	:

Negative examples:

	Anything other than the above


17. Punct comma

Positive examples:

	,

Negative examples:

	Anything other than the above


18. Typename

Positive examples:

	integer
	boolean

Negative examples:

	Anything other than the above	
