import sys

def is_prime(num, i):
    if (i < num):
        if (num % i) == 0:
            return False
        else:
            return is_prime(num, i + 1)
    else:
        return True

def list_primes(lower, upper):
    if (lower < upper):
        if is_prime(lower, 2):
            print(lower)
            list_primes(lower + 1, upper)
        else:
            list_primes(lower + 1, upper)
    else:
        return 0

def main():
    lower = sys.argv[1]
    upper = sys.argv[2]

    list_primes(lower, upper)

if __name__ == "__main__":
    main()
