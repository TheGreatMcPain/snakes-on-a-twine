# Snakes on a Twine

A [Twine](http://www.cs.uni.edu/~wallingf/teaching/cs4550/compiler/specification.html) compiler written in Python

### By Troy Miller, James McClain, and Ju Hann Goh

## Project Description

#### Post-Compiler 7 changes

* Fixed `AttributeError` due to a typo in code generator.

* Fixed `get_used_funcs()` getting stuck in a recursive loop when twine program has a recursive function.

* Fixed index error in scanner when the twine comment symbol `%` is placed right after an identifier token.

* Update `twinef` due to a change in `parser()` arguments.

#### Compiler 7 changes (See docs/OLD_CHANGELOG.md for Compiler 6 changes.)

* Turns out the `|` operation is an "inclusive or". Making it behave correctly fixes `sieve.twn`, and other programs that use `|`.

* Code generator will not generate code for functions that are never called.

* Fixed inner function calls not getting priority when processing function call arguments.

#### Known Bugs

Nothing as of Compiler 7

#### Currently Missing Language Features

Unless we missed something all the language features should be there.

#### Optimizations

* Optimize output TM code filesize by removing dead code.

## Dependencies

Works with Python 3.9, but might work with older versions of Python3.

## Usage

There are 5 scripts included which demonstrate the stages of our compiler.

The scripts are written in Python, so they can be easily run on other platforms that Python supports.

| Script   | Description                                                               |
|:--------:|:--------------------------------------------------------------------------|
| `twines` | Runs the scanner phase and prints the list of tokens.                     |
| `twinef` | Runs the parser phase and prints "Valid Program" if the program is valid. |
| `twinep` | Runs the parser phase and prints the Abstract Syntax Tree.                |
| `twinev` | Runs the syntactic analyzer and prints the Symbol Table.                  |
| `twinec` | Runs the code generator and outputs TM code to a file.                    |

Most scripts are run like so. `./twine(s,f,p,v,c) <twine file>`

The `twinec` program has a `-d` flag which follows the input file name; this prints the input AST, the stage1 code, stage2 code, and the final TM file.

## Implementation

Here's a brief overview of the files in `src`.

| File                                   | Description                                                                  |
|:---------------------------------------|:-----------------------------------------------------------------------------|
| `src/data_structs`                     | data structures used by the various parts of the compiler                    |
| `src/data_structs/twine_errors.py`     | error handling functions                                                     |
| `src/data_structs/token_twine.py`      | has the Token object                                                         |
| `src/data_structs/ast_nodes.py`        | the Abstract Syntax Tree                                                     |
| `src/data_structs/symbol_table.py`     | the Symbol Table                                                             |
| `src/parser_utils`                     | helper functions and data structures used by the parser                      |
| `src/parser_utils/grammer.py`          | has the twine language grammer stored as a Python object                     |
| `src/parser_utils/parse_table.py`      | has the parse table derived from the language grammer and a first-follow set |
| `src/parser_utils/semantic_actions.py` | functions that create the AST nodes which make up the AST                    |
| `src/code_gen_utils`                   | data structures and functions used by code generator                         |
| `src/code_gen_utils/code_emitters.py`  | StackFrame and Register objects and code emitters                            |
| `src/scanner.py`                       | Code for the scanner phase.                                                  |
| `src/parser.py`                        | Code for the parser phase.                                                   |
| `src/semantic_analyzer.py`             | Code for syntactic analyzer.                                                 |
| `src/code_generator.py`                | Code generator.                                                              |

You may go "here" (TODO: make in-depth code doc) for a more in-depth explanation of our compiler.
