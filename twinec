#!/usr/bin/env python3
from src.data_structs import symbol_table
from src.code_generator import CodeGenerator
from src.optimizer import Optimizer
from src.scanner import Scanner
from src.parser import parse
from src.semantic_analyzer import Semantic_Analyzer
import time
import os
import sys


def main():
    # Check for -d argument.
    if len(sys.argv) < 2:
        print("Usage: {} <filename> (-d)".format(sys.argv[0]))
        print("")
        print("    -d: (optional) Enable debug output.")

    debug = False
    optimize_mode = 0
    if len(sys.argv) >= 3:
        if sys.argv[2] == "-d":
            debug = True

        if sys.argv[2] == "-1":
            optimize_mode = 1
        elif sys.argv[2] == "-2":
            optimize_mode = 2
        elif sys.argv[2] == "-3":
            optimize_mode = 3

    if len(sys.argv) == 4:
        if sys.argv[3] == "-1":
            optimize_mode = 1
        elif sys.argv[3] == "-2":
            optimize_mode = 2
        elif sys.argv[3] == "-3":
            optimize_mode = 3

    filename = sys.argv[1]

    program = ""
    with open(filename, "r") as f:
        program = f.read()

    scanner = Scanner(program, filename)
    ast = parse(scanner)
    semantic_analyzer = Semantic_Analyzer(ast)
    generator = CodeGenerator(semantic_analyzer)
    optimizer = Optimizer(generator.get_ast(), generator.get_symbol_table())

    if debug:
        print("DEBUG: Abstract Syntax Tree")
        print()
        print(generator.get_ast())
        print()

    if optimize_mode > 0:
        print("Optimizing Abstract Syntax Tree")
        if optimize_mode in [1, 3]:
            print("Enabled: Pre-calc Expressions")
        if optimize_mode in [2, 3]:
            print("Enabled: Function inlining")
        optimizer.optimize_ast(optimize_mode)
        print()

        if debug:
            print("DEBUG: Optimized Abstract Syntax Tree")
            print()
            print(generator.get_ast())
            print()

    timer_start = time.time()
    generator.generate_stage1()
    timer_end = time.time()
    print(
        "Generating Stage 1 intermediate code took: {}".format(timer_end - timer_start)
    )

    if debug:
        print()
        print("DEBUG: Stage1 'intermediate' code")
        generator.print_intermediate_stage1()
        print()

    timer_start = time.time()
    generator.generate_stage2()
    timer_end = time.time()
    print("Generating Stage 2 TM code took: {}".format(timer_end - timer_start))

    if debug:
        print()
        print("DEBUG: Stage2 'intermediate' TM code")
        generator.print_intermediate_stage2()
        print()

    timer_start = time.time()
    final_code = ""
    final_code = generator.generate_final()
    timer_end = time.time()
    print("Generating Final TM code took: {}".format(timer_end - timer_start))
    print()

    if debug:
        print("DEBUG: Final Stage TM code")
        print()
        print(final_code)

    if not final_code:
        print("Something bad happened while outputting code to file.")
        return

    output_file = os.path.basename(filename).split(".twn")[0] + ".tm"

    print("Wrote TM code to {}".format(output_file))

    with open(output_file, "w") as f:
        f.write(final_code)


if __name__ == "__main__":
    main()
