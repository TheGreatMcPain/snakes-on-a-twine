from src.data_structs.ast_nodes import Error_Type
from src.data_structs.twine_errors import TwineSemanticError
from src.data_structs.symbol_table import Symbol_Table


class Semantic_Analyzer:
    def __init__(self, ast):
        self.ast = ast

    def analyze(self, optimizer = None):
        symbol_table = Symbol_Table()
        self.ast.set_type(symbol_table, None)

        # Ignore errors when using optimizer
        if optimizer:
            return self.ast, symbol_table

        if type(self.ast.get_type()) == Error_Type:
            error_msg = ""
            if not symbol_table.is_entry("main"):
                error_msg += " Missing main function"
            raise TwineSemanticError(error_msg + self.ast.get_type().get_msg())

        if not symbol_table.is_entry("main"):
            error_msg = " Missing main function"
            raise TwineSemanticError(error_msg)

        return self.ast, symbol_table
