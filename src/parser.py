from src.data_structs.token_twine import TokenType
# from src.parser_utils.parse_table import gen_parse_table
from src.parser_utils.table_generator import gen_parse_table
from src.parser_utils.grammar import grammar
from src.scanner import Scanner
from src.data_structs.twine_errors import TwineSyntacticError, TwineEofError
from src.parser_utils.semantic_actions import *

# Stack operations
def top(stack):
    return stack[-1]


def pop(stack):
    return stack.pop()


def push(element, stack):
    stack.append(element)
    return stack


# Parse a given program
def parse(scanner: Scanner):
    parse_table = gen_parse_table()
    parser_stack = []
    semantic_stack = []
    matched_terminal = None

    parser_stack.append(TokenType.end_of_file)
    parser_stack.append("PROGRAM")

    # scanner = Scanner(program, filename)

    token_value = top(parser_stack)
    parens = 0 # Tracks depth of parens

    while token_value != TokenType.end_of_file:
        token_value = top(parser_stack)

        # print("P-Stack: " + str(parser_stack))
        # print("Next Token: " + str(scanner.peek()))
        # print("S-Stack: " + str(semantic_stack))
        # print("Matched Token: " + str(matched_terminal) + "\n")
        # print("Token Value:", token_value)

        # if token_value not in terminals:
        if token_value == "ε":
            # Pop "ε" and move on
            pop(parser_stack)

        elif type(token_value) == TokenType:
            if token_value == scanner.peek().token_type:
                pop(parser_stack)
                token = scanner.next()
                if token.token_type == TokenType.punct_leftparen:
                    parens += 1
                if token.token_type == TokenType.punct_rightparen:
                    parens -= 1

                if (
                    token.isId()
                    or token.isTypenameBoolean()
                    or token.isTypenameInteger()
                    or token.isInteger()
                    or token.isBoolean()
                    or token.isIdPrint()
                ):
                    matched_terminal = token

            else:
                error_msg = "Expected {}, found {}: {}"
                raise TwineSyntacticError(
                    scanner.peek(),
                    error_msg.format(
                        token_value, scanner.peek().token_type, scanner.peek().value()
                    ),
                )

        elif type(token_value) == SemanticAction:
            action = action_table[token_value]
            if (
                action == make_id
                or action == make_type
                or action == make_bool
                or action == make_int
                or action == make_name
            ):
                semantic_stack = action(
                    matched_terminal, matched_terminal.value(), semantic_stack
                )
            else:
                # print("Action: " + str(action) + "\nPre-Stack: " + str(semantic_stack))
                if action in [make_add, make_subtract, make_divide, make_multiply]:
                    semantic_stack = action(semantic_stack, parens)
                else:
                    semantic_stack = action(semantic_stack)

            pop(parser_stack)

            # print("Post-Stack: " + str(semantic_stack))
            # print(top(semantic_stack))
            # print("\n")

        else:
            if parse_table[token_value][scanner.peek().token_type]:
                pop(parser_stack)

                symbol_list = parse_table[token_value][scanner.peek().token_type][:]

                while len(symbol_list) > 0:
                    push(symbol_list.pop(), parser_stack)

            else:
                print("YEET: {} {} {}".format(token_value, scanner.peek().token_type, parse_table[token_value][scanner.peek().token_type]))
                error_msg = "No transition out of non-terminal {} for {}"
                raise TwineSyntacticError(
                    scanner.peek(),
                    error_msg.format(token_value, scanner.peek().token_type),
                )

        # if len(semantic_stack) > 0:
        #     print(top(semantic_stack))
        #     print("\n")

    if not scanner.peek().isEof():
        error_msg = "Unexpected token at end: {}"
        raise TwineEofError(error_msg.format(scanner.peek()))

    if len(semantic_stack) != 1:
        error_msg = "Unexpected number of AST nodes: {}"
        raise TwineEofError(error_msg.format(semantic_stack))

    return top(semantic_stack)
