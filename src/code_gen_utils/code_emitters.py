# Code emitters for code generator stage 2.
from src.data_structs.symbol_table import Symbol_Table
from src.code_gen_utils.stackframe_registers import Registers, StackFrame

#
# Methods for translating intermediate language to TM.
#
def setup_dmem_to_tm(stack_frame: StackFrame):
    registers = Registers()
    setup_dmem_code = []
    if stack_frame.get_temp_count() > 0:
        setup_dmem_code.append(
            (
                "LDA",
                registers.dmem_top,
                stack_frame.get_temp_count(),
                registers.dmem_top,
                "increase dmem top for temp variables.",
            )
        )
    return setup_dmem_code


def param_to_tm(
    stack_frame: StackFrame,
    symbol_table: Symbol_Table,
    function_name: str,
    instruction: tuple,
):
    registers = Registers()
    param_code = []
    table_entry = symbol_table.get_entry(function_name)
    param_index = table_entry.get_param_index(instruction[2])
    param_code.append(
        (
            "LD",
            instruction[3],
            stack_frame.get_argument_addr(param_index),
            registers.dmem_top,
            "r{} = {}".format(
                instruction[3],
                instruction[2],
                stack_frame.get_argument_addr(param_index),
            ),
        )
    )
    return param_code


def out_to_tm(instruction: tuple):
    registers = Registers()
    out_code = []
    out_code.append(
        (
            "OUT",
            instruction[3],
            registers.zero,
            registers.zero,
            "stdout = r{}".format(instruction[3]),
        )
    )
    return out_code


def func_call_to_tm(
    calling_stack_frame: StackFrame,
    stack_frame: StackFrame,
    instruction: tuple,
    calling_function_name: str,
):
    registers = Registers()
    calling_stack_frame.set_called_stack_frame(stack_frame)
    function_call_code = []
    function_call_code.append(
        (
            "LDA",
            registers.return_addr,
            3,
            registers.program_counter,
            "r5 = return address",
        )
    )
    function_call_code.append(
        (
            "ST",
            registers.return_addr,
            stack_frame.get_calling_return_addr(calling_stack_frame),
            registers.dmem_top,
            "{}'s return address = r5".format(calling_function_name),
        )
    )
    function_call_code.append(
        (
            "LDA",
            registers.dmem_top,
            stack_frame.get_calling_return_addr(calling_stack_frame) + 1,
            registers.dmem_top,
            "dmem top = top of {}'s stackframe".format(calling_function_name),
        )
    )
    function_call_code.append(
        (
            "LDA",
            registers.program_counter,
            calling_function_name,
            registers.zero,
            "Goto '{}'".format(calling_function_name),
        )
    )
    function_call_code.append(
        (
            "LD",
            instruction[2],
            stack_frame.get_calling_return_addr(calling_stack_frame) - 1,
            registers.dmem_top,
            "r{} = {}'s return value".format(instruction[2], calling_function_name),
        )
    )

    return function_call_code


def math_to_tm(instruction: tuple):
    operation_map = {
        "add": "ADD",
        "subtract": "SUB",
        "multiply": "MUL",
        "divide": "DIV",
    }
    math_code = []
    math_code.append(
        (
            operation_map[instruction[0]],
            instruction[3],
            instruction[1],
            instruction[2],
            "{} = r{} {} r{}".format(
                instruction[3],
                instruction[1],
                instruction[0],
                instruction[2],
            ),
        )
    )
    return math_code


def is_lt_zero_to_tm(instruction: tuple):
    registers = Registers()
    is_lt_zero_code = []
    is_lt_zero_code.append(
        (
            "JGE",
            instruction[3],
            2,
            registers.program_counter,
            "Jump if r{} >= 0".format(instruction[3]),
        )
    )
    is_lt_zero_code.append(
        (
            "LDC",
            instruction[3],
            1,
            registers.zero,
            "r{} = true 'if r{} was less than 0'".format(
                instruction[3], instruction[3]
            ),
        )
    )
    is_lt_zero_code.append(
        (
            "LDA",
            registers.program_counter,
            1,
            registers.program_counter,
            "jump over else code",
        )
    )
    is_lt_zero_code.append(
        (
            "LDC",
            instruction[3],
            0,
            registers.zero,
            "r{} = false 'if r{} was >= 0'".format(instruction[3], instruction[3]),
        )
    )
    return is_lt_zero_code


def is_zero_to_tm(instruction: tuple):
    registers = Registers()
    is_zero_code = []
    is_zero_code.append(
        (
            "JNE",
            instruction[3],
            2,
            registers.program_counter,
            "Jump if r{} != 0".format(instruction[3]),
        )
    )
    is_zero_code.append(
        (
            "LDC",
            instruction[3],
            1,
            registers.zero,
            "r{} = true 'if r{} was 0'".format(instruction[3], instruction[3]),
        )
    )
    is_zero_code.append(
        (
            "LDA",
            registers.program_counter,
            1,
            registers.program_counter,
            "jump over else code",
        )
    )
    is_zero_code.append(
        (
            "LDC",
            instruction[3],
            0,
            registers.zero,
            "r{} = false 'if r{} was not 0'".format(instruction[3], instruction[3]),
        )
    )
    return is_zero_code


def comparison_to_tm(instruction: tuple):
    comparison_code = []
    # Subtract left and right if it's equals then
    # the result should be zero.
    if instruction[0] != "or":
        comparison_code += math_to_tm(
            (
                "subtract",
                instruction[1],
                instruction[2],
                instruction[3],
            )
        )
    else:
        comparison_code += math_to_tm(
            (
                "add",
                instruction[1],
                instruction[2],
                instruction[3],
            )
        )
    if instruction[0] in ["equals", "and"]:
        comparison_code += is_zero_to_tm(
            (
                "is_zero",
                None,
                None,
                instruction[3],
            )
        )
    if instruction[0] == "or":
        comparison_code += is_not_zero_to_tm(
            (
                "is_not_zero",
                None,
                None,
                instruction[3],
            )
        )
    if instruction[0] == "less_than":
        comparison_code += is_lt_zero_to_tm(
            (
                "is_lt_zero",
                None,
                None,
                instruction[3],
            )
        )
    return comparison_code


def is_not_zero_to_tm(instruction: tuple):
    registers = Registers()
    is_not_zero_code = []
    is_not_zero_code.append(
        (
            "JEQ",
            instruction[3],
            2,
            registers.program_counter,
            "Jump if r{} = 0".format(instruction[3]),
        )
    )
    is_not_zero_code.append(
        (
            "LDC",
            instruction[3],
            1,
            registers.zero,
            "r{} = true 'if r{} was not 0'".format(instruction[3], instruction[3]),
        )
    )
    is_not_zero_code.append(
        (
            "LDA",
            registers.program_counter,
            1,
            registers.program_counter,
            "jump over else code",
        )
    )
    is_not_zero_code.append(
        (
            "LDC",
            instruction[3],
            0,
            registers.zero,
            "r{} = false 'if r{} was 0'".format(instruction[3], instruction[3]),
        )
    )
    return is_not_zero_code


def if_then_to_tm(instruction: tuple):
    registers = Registers()
    if_then_code = []
    if_then_code.append(
        (
            "JEQ",
            instruction[1],
            instruction[4],
            registers.program_counter,
            "if r{} is false jump to else".format(instruction[1]),
            instruction[1],
        )
    )
    return if_then_code


def else_to_tm(instruction: tuple):
    registers = Registers()
    else_code = []
    else_code.append(
        (
            "LDA",
            registers.program_counter,
            instruction[4],
            registers.program_counter,
            "jump over else code",
            instruction[3],
        )
    )
    return else_code


def end_if_to_tm(instruction: tuple):
    registers = Registers()
    end_if_code = []
    end_if_code.append(
        (
            "LDA",
            registers.program_counter,
            registers.zero,
            registers.program_counter,
            "Marker for end_if",
            instruction[4],
        )
    )
    return end_if_code


def negative_to_tm(instruction: tuple):
    negative_code = []
    empty_register = instruction[2]
    negative_code += math_to_tm(
        (
            "subtract",
            instruction[1],
            instruction[1],
            empty_register,
        )
    )
    negative_code += math_to_tm(
        (
            "subtract",
            instruction[3],
            empty_register,
            instruction[1],
        )
    )
    return negative_code


def not_to_tm(instruction: tuple):
    not_code = []
    not_code += is_zero_to_tm(("is_zero", None, None, instruction[3]))
    return not_code


def boolean_to_tm(instruction: tuple):
    registers = Registers()
    boolean = None
    if instruction[2] == 0:
        boolean = "false"
    else:
        boolean = "true"
    integer_code = []
    integer_code.append(
        (
            "LDC",
            instruction[3],
            instruction[2],
            registers.zero,
            "r{} = '{}'".format(instruction[3], boolean),
        )
    )
    return integer_code


def integer_to_tm(instruction: tuple):
    registers = Registers()
    integer_code = []
    integer_code.append(
        (
            "LDC",
            instruction[3],
            instruction[2],
            registers.zero,
            "r{} = {}".format(instruction[3], instruction[2]),
        )
    )
    return integer_code


def return_to_tm(stack_frame: StackFrame, function_name: str, instruction: tuple):
    registers = Registers()
    return_code = []
    return_code.append(
        (
            "ST",
            instruction[3],
            stack_frame.get_return_value(),
            registers.dmem_top,
            "{}'s return value = r{}".format(function_name, instruction[3]),
        )
    )
    return_code.append(
        (
            "LD",
            registers.return_addr,
            stack_frame.get_return_addr(),
            registers.dmem_top,
            "r{} = {}'s return address".format(registers.return_addr, function_name),
        )
    )
    return_code.append(
        (
            "LDA",
            registers.dmem_top,
            stack_frame.get_prev_top(),
            registers.dmem_top,
            "deallocate {}'s stack frame".format(function_name),
        )
    )
    return_code.append(
        (
            "LDA",
            registers.program_counter,
            registers.zero,
            registers.return_addr,
            "Goto return address",
        )
    )
    return return_code


def arg_to_tm(stack_frame: StackFrame, instruction: tuple):
    registers = Registers()
    arg_code = []
    arg_code.append(
        (
            "ST",
            instruction[2],
            stack_frame.get_calling_arg_addr(instruction[3]),
            registers.dmem_top,
            "{}'s argument #{} = r{}".format(
                instruction[1], instruction[3], instruction[2]
            ),
        )
    )
    return arg_code


def store_temp_to_tm(stack_frame: StackFrame, instruction: tuple):
    registers = Registers()
    store_temp_code = []
    store_temp_code.append(
        (
            "ST",
            instruction[2],
            stack_frame.get_temp_addr(instruction[3]),
            registers.dmem_top,
            "{} = r{}".format(instruction[3], instruction[2]),
        )
    )
    return store_temp_code


def load_temp_to_tm(stack_frame: StackFrame, instruction: tuple):
    registers = Registers()
    load_temp_code = []
    load_temp_code.append(
        (
            "LD",
            instruction[3],
            stack_frame.get_temp_addr(instruction[2]),
            registers.dmem_top,
            "r{} = {}".format(instruction[3], instruction[2]),
        )
    )
    return load_temp_code
