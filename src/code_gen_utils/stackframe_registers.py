from src.data_structs.symbol_table import Symbol_Table_Entry

# An abstraction of a temp variable.
# Will make it easy to track unique variables.
class TempVariable:
    def __init__(self, stack_frame):
        self.stack_frame = stack_frame
        self.value = None

    def set_value(self, value):
        self.value = value

    def get_value(self):
        return self.value

    def get_index(self):
        return self.stack_frame.get_temp_index(self)

    def __str__(self):
        return "t_{}".format(self.get_index())


# Abstraction for runtime stack addresses.
# Since we store the runtime stack pointer in a register we just need to
# calculate address locations based on the number of parameters.
class StackFrame:
    def __init__(self, symbol_table_entry: Symbol_Table_Entry):
        self.number_of_parameters = len(symbol_table_entry.get_params().keys())
        # list of tmep vars.
        self.temp_vars = []

        self.called_by_stack_frame = None

    # Mainly used for init code, because of main's parameters
    def initial_return_addr(self):
        return self.number_of_parameters + 1

    def set_called_stack_frame(self, called_stack_frame):
        self.called_by_stack_frame = called_stack_frame

    def get_calling_return_addr(self, calling_stack_frame):
        if isinstance(calling_stack_frame, type(self)):
            return calling_stack_frame.initial_return_addr()
        return -1

    def get_calling_arg_addr(self, index):
        return index

    def get_argument_addr(self, index):
        return -(self.number_of_parameters + self.get_temp_count() + 2) + index

    def get_return_value(self):
        return -(self.get_temp_count() + 2)

    def get_return_addr(self):
        return -(self.get_temp_count() + 1)

    def get_prev_top(self):
        return -(self.number_of_parameters + self.get_temp_count() + 2)

    def create_temp(self):
        temp_var = TempVariable(self)
        self.temp_vars.append(temp_var)
        return temp_var

    # See if a temp var has a specific value.
    def is_in_temp(self, value):
        for var in self.temp_vars:
            if var.get_value() == value:
                return True
        return False

    # Get var based on value.
    def get_temp_by_value(self, value):
        for var in self.temp_vars:
            if var.get_value() == value:
                return var
        return None

    def remove_temp(self, temp_var: TempVariable):
        self.temp_vars.remove(temp_var)

    def get_temp_addr(self, temp_var: TempVariable):
        return -(self.get_temp_index(temp_var) + 1)

    def get_temp_index(self, temp_var: TempVariable):
        return self.temp_vars.index(temp_var)

    def get_temp_count(self):
        return len(self.temp_vars)

    def reset_temps(self):
        self.temp_vars = []


class Registers:
    def __init__(self):
        self.zero = 0
        self.free_registers = [None, None, None, None]
        self.return_addr = 5
        self.dmem_top = 6
        self.program_counter = 7

        self.store_register_counter = 0

    # Allocate a value with a free register.
    # Return the register number
    def store_in_register(self, store_me):
        if self.store_register_counter == len(self.free_registers) - 1:
            self.store_register_counter = 0
        self.free_registers[self.store_register_counter] = store_me
        self.store_register_counter += 1
        return self.store_register_counter

    # De-allocate a register and return it.
    def get_register(self):
        if self.store_register_counter == len(self.free_registers) - 1:
            self.store_register_counter = 0
        self.free_registers[self.store_register_counter] = None
        self.store_register_counter += 1
        return self.store_register_counter

    # Get a value associated register.
    def load_from_register(self, value):
        if value in self.free_registers:
            return self.free_registers.index(value) + 1
        return None

    # De-associate a value from it's register, and return the register index.
    def pop_from_register(self, value):
        index = self.free_registers.index(value)
        self.free_registers[index] = None
        return index + 1
