from src.data_structs.token_twine import Token, TokenType
from enum import Enum
from src.data_structs.twine_errors import TwineLexicalError
import os


class State(Enum):
    looking = 1

    # Integer
    zero = 0
    integer = 2

    # Symbols
    leftparen = 3
    rightparen = 4
    colon = 5
    comma = 6

    # Operators
    op_not = 7
    op_and = 8
    op_or = 9
    op_lessthan = 10
    op_divide = 11
    op_multiply = 12
    op_minus = 13
    op_plus = 14
    op_equals = 15

    # Ids_And_Reserved
    ids_and_reserved = 16


# Used by Scanner in an attempt to reduce the
# Amount of code used.
class _SelfDelimitingSymbols:
    def __init__(self):
        self.symbol_dict = {
            "(": (TokenType.punct_leftparen, State.leftparen),
            ")": (TokenType.punct_rightparen, State.rightparen),
            ":": (TokenType.punct_colon, State.colon),
            ",": (TokenType.punct_comma, State.comma),
            "~": (TokenType.op_not, State.op_not),
            "^": (TokenType.op_and, State.op_and),
            "|": (TokenType.op_or, State.op_or),
            "<": (TokenType.op_lessthan, State.op_lessthan),
            "/": (TokenType.op_divide, State.op_divide),
            "*": (TokenType.op_multiply, State.op_multiply),
            "-": (TokenType.op_minus, State.op_minus),
            "+": (TokenType.op_plus, State.op_plus),
            "=": (TokenType.op_equals, State.op_equals),
        }

    def get_symbols(self):
        return self.symbol_dict.keys()

    def get_token_types(self):
        # Return a list of TokenTypes for the symbols.

        # Accoring to: https://stackoverflow.com/a/20816744
        # "List comprehension" is really fast.
        return [self.symbol_dict[x][0] for x in self.symbol_dict.keys()]

    def get_states(self):
        # Return a list of States for the symbols.
        return [self.symbol_dict[x][1] for x in self.symbol_dict.keys()]

    def get_token_type(self, symbol: str):
        return self.symbol_dict[symbol][0]

    def get_state(self, symbol: str):
        return self.symbol_dict[symbol][1]


class Scanner:
    def __init__(self, program: str, filename: str = None):
        self.tokens = []
        self.accum = ""
        self.filename = filename

        # If filename is actually set lets make sure we just
        # use the basename without the extra path info.
        if self.filename:
            self.filename = os.path.basename(self.filename)

        self.self_delimit_syms = _SelfDelimitingSymbols()

        self.state = State.looking
        self.pointer = 0
        self.__temp_pointer = 0  # Used by peek and next

        self.program = program
        if len(self.program) == 0:
            self.program += "\n"

        if not self.program[-1].isspace():
            self.program += "\n"

        # Setup a list of "indexes" containing newline occurances.
        self.newlines = []
        for index in range(0, len(self.program)):
            if self.program[index] == "\n":
                self.newlines.append(index)

    def scan(self):
        token = self.next()
        self.tokens.append(token)

        while not token.isEof():
            token = self.next()
            self.tokens.append(token)

        return self.tokens

    def get_current_char_no(self):
        start_of_current_line = 0
        if (self.get_current_line_no()) > 1:
            start_of_current_line = self.newlines[self.get_current_line_no() - 2]
        return (self.__temp_pointer - start_of_current_line) - 1

    def get_current_line_no(self):
        # Get the line number based on the position of the pointer,
        # and our pre-processed list of newline character positions.
        for index in range(0, len(self.newlines)):
            if self.newlines[index] >= self.__temp_pointer:
                return index + 1
        return len(self.newlines)

    def get_current_line(self):
        start_of_current_line = 0
        if (self.get_current_line_no()) > 1:
            start_of_current_line = self.newlines[self.get_current_line_no() - 2] + 1
        end_of_current_line = self.newlines[self.get_current_line_no() - 1]
        return self.program[start_of_current_line:end_of_current_line]

    # Return next token, but don't update pointer
    def peek(self):
        self.__temp_pointer = self.pointer
        token = None

        while not token:
            # For debugging
            # print("Pos:", self.__temp_pointer, "State:", self.state)

            if self.state == State.looking:
                # Exit loop of end of file.
                if self.__temp_pointer >= len(self.program):
                    token = Token(
                        TokenType.end_of_file,
                        "$",
                        self.get_current_line_no(),
                        self.get_current_char_no(),
                        self.get_current_line(),
                    )
                    break

                if self.program[self.__temp_pointer].isspace():
                    if self.program[self.__temp_pointer] == "\n":
                        self.state = State.looking

                elif self.program[self.__temp_pointer] in "%":
                    self.__temp_pointer = self.program.find("\n", self.__temp_pointer)
                    self.state = State.looking

                elif self.program[self.__temp_pointer] in "0":
                    self.accum = self.program[self.__temp_pointer]
                    self.state = State.zero

                elif self.program[self.__temp_pointer] in "123456789":
                    self.accum = self.program[self.__temp_pointer]
                    self.state = State.integer

                elif (
                    self.program[self.__temp_pointer]
                    in self.self_delimit_syms.get_symbols()
                ):
                    symbol = self.program[self.__temp_pointer]
                    self.accum = symbol
                    self.state = self.self_delimit_syms.get_state(symbol)

                elif (
                    self.program[self.__temp_pointer].isalpha()
                    or self.program[self.__temp_pointer] in "$"
                ):
                    self.accum = self.program[self.__temp_pointer]
                    self.state = State.ids_and_reserved

                else:
                    error_msg = "Invalid character at beginning of identifier: {}"
                    raise TwineLexicalError(
                        self.filename,
                        self.get_current_line_no(),
                        self.get_current_char_no(),
                        self.get_current_line(),
                        "looking",
                        error_msg.format(self.program[self.__temp_pointer]),
                    )

                self.__temp_pointer += 1

            elif self.state == State.zero:
                if (
                    self.program[self.__temp_pointer].isspace()
                    or self.program[self.__temp_pointer]
                    in self.self_delimit_syms.get_symbols()
                ):

                    token = Token(
                        TokenType.int_token,
                        0,
                        self.get_current_line_no(),
                        self.get_current_char_no(),
                        self.get_current_line(),
                    )

                elif self.program[self.__temp_pointer] in "%":
                    self.__temp_pointer = self.program.find("\n", self.__temp_pointer)
                    self.state = State.looking

                    token = Token(
                        TokenType.int_token,
                        0,
                        self.get_current_line_no(),
                        self.get_current_char_no(),
                        self.get_current_line(),
                    )

                elif self.program[self.__temp_pointer].isdigit():
                    error_msg = "Integers do not have leading zeros: 0{}"
                    raise TwineLexicalError(
                        self.filename,
                        self.get_current_line_no(),
                        self.get_current_char_no(),
                        self.get_current_line(),
                        "zero",
                        error_msg.format(self.program[self.__temp_pointer]),
                    )
                else:
                    error_msg = "Invalid character after a 0: 0*{}*"
                    raise TwineLexicalError(
                        self.filename,
                        self.get_current_line_no(),
                        self.get_current_char_no(),
                        self.get_current_line(),
                        "zero",
                        error_msg.format(self.program[self.__temp_pointer]),
                    )
                self.accum = ""
                self.state = State.looking

                if (
                    not self.program[self.__temp_pointer]
                    in self.self_delimit_syms.get_symbols()
                ):
                    self.__temp_pointer += 1

            elif self.state == State.integer:
                if self.program[self.__temp_pointer].isdigit():
                    self.accum += self.program[self.__temp_pointer]

                elif (
                    self.program[self.__temp_pointer].isspace()
                    or self.program[self.__temp_pointer]
                    in self.self_delimit_syms.get_symbols()
                ):
                    if int(self.accum) > 2147483647:
                        error_msg = "Integer must be smaller than 2147483648"
                        raise TwineLexicalError(
                            self.filename,
                            self.get_current_line_no(),
                            self.get_current_char_no(),
                            self.get_current_line(),
                            "integer",
                            error_msg,
                        )

                    token = Token(
                        TokenType.int_token,
                        int(self.accum),
                        self.get_current_line_no(),
                        self.get_current_char_no(),
                        self.get_current_line(),
                    )

                    self.accum = ""
                    self.state = State.looking

                elif self.program[self.__temp_pointer] in "%":
                    self.__temp_pointer = self.program.find("\n", self.__temp_pointer)
                    self.state = State.looking

                    token = Token(
                        TokenType.int_token,
                        int(self.accum),
                        self.get_current_line_no(),
                        self.get_current_char_no(),
                        self.get_current_line(),
                    )
                    self.accum = ""

                else:
                    error_msg = "Invalid character in integer {}*{}*"
                    raise TwineLexicalError(
                        self.filename,
                        self.get_current_line_no(),
                        self.get_current_char_no(),
                        self.get_current_line(),
                        "integer",
                        error_msg.format(self.accum, self.program[self.__temp_pointer]),
                    )

                if (
                    not self.program[self.__temp_pointer]
                    in self.self_delimit_syms.get_symbols()
                ):
                    self.__temp_pointer += 1

            elif self.state in self.self_delimit_syms.get_states():
                token = Token(
                    self.self_delimit_syms.get_token_type(self.accum),
                    self.accum,
                    self.get_current_line_no(),
                    self.get_current_char_no(),
                    self.get_current_line(),
                )
                self.accum = ""
                self.state = State.looking

            elif self.state == State.ids_and_reserved:
                if (
                    self.program[self.__temp_pointer].isspace()
                    or self.program[self.__temp_pointer]
                    in self.self_delimit_syms.get_symbols()
                ):
                    if len(self.accum) > 256:
                        error_msg = "Identifier exceeds 256 character limit."
                        raise TwineLexicalError(
                            self.filename,
                            self.get_current_line_no(),
                            self.get_current_char_no(),
                            self.get_current_line(),
                            "identifiers and reserved words",
                            error_msg,
                        )

                    if self.accum == "if":
                        token = Token(
                            TokenType.keyword_if,
                            self.accum,
                            self.get_current_line_no(),
                            self.get_current_char_no(),
                            self.get_current_line(),
                        )
                    elif self.accum == "else":
                        token = Token(
                            TokenType.keyword_else,
                            self.accum,
                            self.get_current_line_no(),
                            self.get_current_char_no(),
                            self.get_current_line(),
                        )
                    elif self.accum == "f":
                        token = Token(
                            TokenType.keyword_f,
                            self.accum,
                            self.get_current_line_no(),
                            self.get_current_char_no(),
                            self.get_current_line(),
                        )
                    elif self.accum == "returns":
                        token = Token(
                            TokenType.keyword_returns,
                            self.accum,
                            self.get_current_line_no(),
                            self.get_current_char_no(),
                            self.get_current_line(),
                        )
                    elif self.accum == "integer":
                        token = Token(
                            TokenType.typename_integer,
                            self.accum,
                            self.get_current_line_no(),
                            self.get_current_char_no(),
                            self.get_current_line(),
                        )
                    elif self.accum == "boolean":
                        token = Token(
                            TokenType.typename_boolean,
                            self.accum,
                            self.get_current_line_no(),
                            self.get_current_char_no(),
                            self.get_current_line(),
                        )
                    elif self.accum == "print":
                        token = Token(
                            TokenType.id_print,
                            self.accum,
                            self.get_current_line_no(),
                            self.get_current_char_no(),
                            self.get_current_line(),
                        )
                    elif self.accum == "true" or self.accum == "false":
                        token = Token(
                            TokenType.bool_token,
                            self.accum,
                            self.get_current_line_no(),
                            self.get_current_char_no(),
                            self.get_current_line(),
                        )
                    else:
                        token = Token(
                            TokenType.id_token,
                            self.accum,
                            self.get_current_line_no(),
                            self.get_current_char_no(),
                            self.get_current_line(),
                        )
                    self.accum = ""

                    self.state = State.looking

                elif self.program[self.__temp_pointer] in "%":

                    if len(self.accum) > 256:
                        error_msg = "Identifier exceeds 256 character limit."
                        raise TwineLexicalError(
                            self.filename,
                            self.get_current_line_no(),
                            self.get_current_char_no(),
                            self.get_current_line(),
                            "identifiers and reserved words",
                            error_msg,
                        )

                    self.__temp_pointer = self.program.find("\n", self.__temp_pointer)

                    self.state = State.looking

                elif self.program[self.__temp_pointer].isalpha():
                    self.accum += self.program[self.__temp_pointer]
                elif self.program[self.__temp_pointer] in "$":
                    self.accum += self.program[self.__temp_pointer]
                elif self.program[self.__temp_pointer] in "_":
                    self.accum += self.program[self.__temp_pointer]
                elif self.program[self.__temp_pointer] in "0123456789":
                    self.accum += self.program[self.__temp_pointer]
                else:
                    error_msg = "Invalid identifier character: {}*{}*"
                    raise TwineLexicalError(
                        self.filename,
                        self.get_current_line_no(),
                        self.get_current_char_no(),
                        self.get_current_line(),
                        "identifiers and reserved words",
                        error_msg.format(self.accum, self.program[self.__temp_pointer]),
                    )

                if (
                    not self.program[self.__temp_pointer]
                    in self.self_delimit_syms.get_symbols()
                ):
                    self.__temp_pointer += 1

        token.setFilename(self.filename)
        return token

    def next(self):
        token = self.peek()
        self.pointer = self.__temp_pointer

        return token
