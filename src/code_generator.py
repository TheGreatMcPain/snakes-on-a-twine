from src.data_structs.twine_errors import TwineCodeGenError
import src.data_structs.ast_nodes as ast_nodes
import src.code_gen_utils.code_emitters as code_emitters
from src.code_gen_utils.stackframe_registers import StackFrame
from src.code_gen_utils.stackframe_registers import Registers, TempVariable
from src.semantic_analyzer import Semantic_Analyzer
from src.data_structs.symbol_table import Symbol_Table

# Using a AST and Symbol Table created from the semantic analyzer
# generate intermediate code which will make it easier to trainslate
# to TM.
class CodeGenerator:
    def __init__(self, semantic_analyzer: Semantic_Analyzer):
        # Initialize AST and Symbol Table
        self.ast: ast_nodes.Definition_List_Node
        self.symbol_table: Symbol_Table
        self.semantic_analyzer = semantic_analyzer
        self.jump_label_counter = 0  # used for creating jump labels

        self.registers = Registers()

        self.ast, self.symbol_table = self.semantic_analyzer.analyze()

        # If for whatever reason the semantic_analyzer fails to find an error
        # stop the compiler.
        if self.ast.get_type() == ast_nodes.Error_Type:
            raise TwineCodeGenError("Errors in Twine Program! Stopping.")

        self.stackframes = {}
        for function in self.symbol_table.get_table().keys():
            self.stackframes[function] = StackFrame(
                self.symbol_table.get_entry(function)
            )

        self.intermediate = {}
        self.code_comments = {}

    def get_intermediate(self):
        return self.intermediate

    # Convert AST leaf node, TempVariable, etc. to a string.
    def __convert_variable_to_string(self, variable):
        if type(variable) == TempVariable:
            return "{}".format(str(variable))
        if type(variable) == ast_nodes.Integer_Lit_Node:
            return "{}".format(variable.get_value())
        if type(variable) == ast_nodes.Boolean_Lit_Node:
            return "{}".format(variable.get_value())
        if type(variable) == ast_nodes.Identifier_Node:
            return "{}".format(variable.get_value())

        # Assume regular integers are register numbers.
        if type(variable) == int:
            return "r{}".format(variable)

        return ""

    # format intermediate code into comments.
    def stage1_code_to_comments(self):
        comments = {}
        binary_op_map = {
            "add": "+",
            "subtract": "-",
            "multiply": "*",
            "divide": "/",
            "less_than": "<",
            "equals": "==",
            "and": "&",
            "or": "|",
        }

        for definition in self.intermediate.keys():
            arg_list = {}
            comments[definition] = []
            for statement in self.intermediate[definition]:
                # Make sure temp variables are formated
                # abit more human readable.
                statement = list(statement)
                for index in range(0, len(statement)):
                    string_from_var = self.__convert_variable_to_string(
                        statement[index]
                    )
                    if statement[0] == "arg" and index == 3:
                        string_from_var = None
                    if string_from_var:
                        statement[index] = string_from_var
                    if statement[index] in binary_op_map.keys():
                        statement[index] = "{}".format(binary_op_map[statement[index]])
                statement = tuple(statement)

                # Format intermediate code into comments.
                if statement[0] in [binary_op_map[x] for x in binary_op_map.keys()]:
                    comments[definition].append(
                        "    {} = {} {} {}".format(
                            statement[3], statement[1], statement[0], statement[2]
                        )
                    )
                else:
                    if statement[0] == "setup_dmem":
                        comments[definition].append("  Increase DMEM for temp vars.")
                    elif statement[0] == "arg":
                        # Will use this for printing the up-coming func-call.
                        arg_list[statement[3]] = statement[2]
                        comments[definition].append(
                            "    {} to {}'s arg #{}".format(
                                statement[2], statement[1], statement[3]
                            )
                        )
                    elif statement[0] == "func_call":
                        # Format function call to show
                        # something like this
                        # 'func(r0, t_1) to r2'
                        message = "{}(".format(statement[3])
                        for arg in arg_list:
                            message += "{}".format(arg_list[arg])
                            if arg != [x for x in arg_list][-1]:
                                message += ", "
                        message += ")"
                        comments[definition].append(
                            "    {} = {}".format(statement[2], message)
                        )
                        arg_list = {}
                    elif statement[0] == "if_then":
                        comments[definition].append(
                            "  {} ({})".format(statement[0], statement[1])
                        )
                    elif statement[0] in ["else", "end_if"]:
                        comments[definition].append(
                            "  {} ({} = {})".format(
                                statement[0], statement[2], statement[1]
                            )
                        )
                    elif statement[0] == "not":
                        comments[definition].append(
                            "    {} = !{}".format(statement[3], statement[1])
                        )
                    elif statement[0] == "negative":
                        comments[definition].append(
                            "    {} = -{}".format(statement[3], statement[1])
                        )
                    elif statement[0] == "return":
                        comments[definition].append("  return {}".format(statement[3]))
                    else:
                        comments[definition].append("    {}".format(statement))
        return comments

    # Pretty print intermediate code.
    def print_intermediate_stage1(self):
        comments = self.stage1_code_to_comments()
        for definition in comments:
            print()
            print("def {}".format(definition))
            for comment in comments[definition]:
                print(comment)

    def print_intermediate_stage2(self):
        for definition in self.intermediate:
            print()
            print("def {}".format(definition))
            for code in self.intermediate[definition]:
                comment_string = "  {} {} {} {}".format(
                    code[0], code[1], code[2], code[3]
                )
                comment_string = "{0: <20} ; {1:}".format(comment_string, code[4])
                print(comment_string)

    def get_ast(self):
        return self.ast

    def get_symbol_table(self):
        return self.symbol_table

    def __stage1_helper_is_leaf(self, input_variable):
        if type(input_variable) in [
            ast_nodes.Identifier_Node,
            ast_nodes.Integer_Lit_Node,
            ast_nodes.Boolean_Lit_Node,
            TempVariable,
        ]:
            return True
        return False

    # Store a temp_var in a register.
    def __stage2_helper_temp_store_register(
        self, input_statement: list, input_statement_index
    ):
        temp_var = input_statement[input_statement_index]

        # If the target_register is a temp variable associate it with
        # a register.
        if type(temp_var) != TempVariable:
            return
        if self.registers.load_from_register(temp_var):
            input_statement[input_statement_index] = self.registers.load_from_register(
                temp_var
            )
        input_statement[input_statement_index] = self.registers.store_in_register(
            temp_var
        )

    # Store temp_var register into dmem
    def __stage2_helper_temp_dmem(self, definition: str, temp_var, from_register=None):
        if type(temp_var) != TempVariable:
            return []

        if not from_register:
            from_register = self.registers.load_from_register(temp_var)

        intermediate_code = (
            "store_temp",
            None,
            from_register,
            temp_var,
        )

        return code_emitters.store_temp_to_tm(
            self.stackframes[definition],
            intermediate_code,
        )

    # Inserts a load instruction for integers, booleans, identifiers, and temp vars.
    def __stage2_helper_load_variable(
        self, definition: str, input_statement: list, input_statement_index
    ):
        input_variable = input_statement[input_statement_index]

        instruction_map = {
            ast_nodes.Identifier_Node: "param",
            ast_nodes.Boolean_Lit_Node: "boolean",
            ast_nodes.Integer_Lit_Node: "integer",
            TempVariable: "load_temp",
        }

        if not self.__stage1_helper_is_leaf(input_variable):
            return []

        # Check if the input_variable is already loaded on a register.
        input_register = None
        if self.registers.load_from_register(input_variable):
            input_register = self.registers.load_from_register(input_variable)
        else:
            input_register = self.registers.store_in_register(input_variable)

        # Override input_statement's value.
        input_statement[input_statement_index] = input_register

        # Start with TempVariable case
        value_to_load = input_variable
        if type(input_variable) == ast_nodes.Integer_Lit_Node:
            value_to_load = input_variable.get_value()
        if type(input_variable) == ast_nodes.Boolean_Lit_Node:
            value_to_load = 1 if input_variable.get_value() == "true" else 0
        if type(input_variable) == ast_nodes.Identifier_Node:
            value_to_load = input_variable.get_value()

        # Generate intermediate code which
        # will be passed to TM code emitter.
        intermediate_code = (
            instruction_map[type(input_variable)],
            None,
            value_to_load,
            input_register,
        )

        if intermediate_code[0] == "load_temp":
            return code_emitters.load_temp_to_tm(
                self.stackframes[definition],
                intermediate_code,
            )
        if intermediate_code[0] == "integer":
            return code_emitters.integer_to_tm(intermediate_code)
        if intermediate_code[0] == "boolean":
            return code_emitters.boolean_to_tm(intermediate_code)
        if intermediate_code[0] == "param":
            return code_emitters.param_to_tm(
                self.stackframes[definition],
                self.symbol_table,
                definition,
                intermediate_code,
            )

    # Create a jump label for if_then_else expressions
    def __stage1_helper_jump_label(self):
        self.jump_label_counter += 1
        return "JUMP_LABEL_{}".format(self.jump_label_counter)

    def __stage1_call_in_exp(self, ast_node):
        term_nodes = [
            ast_nodes.Add_Node,
            ast_nodes.Subtract_Node,
            ast_nodes.Divide_Node,
            ast_nodes.Multiply_Node,
            ast_nodes.Equals_Node,
            ast_nodes.Less_Than_Node,
            ast_nodes.Or_Node,
            ast_nodes.And_Node,
        ]
        exp_nodes = [
            ast_nodes.Equals_Node,
            ast_nodes.Less_Than_Node,
        ]
        not_negative = [ast_nodes.Not_Node, ast_nodes.Negative_Node]

        if type(ast_node) == ast_nodes.Function_Call_Node:
            return True

        if type(ast_node) in exp_nodes:
            if type(ast_node.get_exp_a()) == ast_nodes.Function_Call_Node:
                return True
            if type(ast_node.get_exp_b()) == ast_nodes.Function_Call_Node:
                return True

            if not self.__stage1_helper_is_leaf(ast_node.get_exp_a()):
                return self.__stage1_call_in_exp(ast_node.get_exp_a())

            if not self.__stage1_helper_is_leaf(ast_node.get_exp_b()):
                return self.__stage1_call_in_exp(ast_node.get_exp_b())

        if type(ast_node) in term_nodes:
            if type(ast_node.get_term_a()) == ast_nodes.Function_Call_Node:
                return True
            if type(ast_node.get_term_b()) == ast_nodes.Function_Call_Node:
                return True

            if not self.__stage1_helper_is_leaf(ast_node.get_term_a()):
                return self.__stage1_call_in_exp(ast_node.get_term_a())

            if not self.__stage1_helper_is_leaf(ast_node.get_term_b()):
                return self.__stage1_call_in_exp(ast_node.get_term_b())

        if type(ast_node) in not_negative:
            if type(ast_node.get_factor()) == ast_nodes.Function_Call_Node:
                return True

            if not self.__stage1_helper_is_leaf(ast_node.get_factor()):
                return self.__stage1_call_in_exp(ast_node.get_factor())

        if type(ast_node) == ast_nodes.If_Node:
            if type(ast_node.get_if_exp()) == ast_nodes.Function_Call_Node:
                return True

            if type(ast_node.get_then_exp()) == ast_nodes.Function_Call_Node:
                return True

            if type(ast_node.get_else_exp()) == ast_nodes.Function_Call_Node:
                return True

            if not self.__stage1_helper_is_leaf(ast_node.get_if_exp()):
                return self.__stage1_call_in_exp(ast_node.get_if_exp())

            if not self.__stage1_helper_is_leaf(ast_node.get_then_exp()):
                return self.__stage1_call_in_exp(ast_node.get_then_exp())

            if not self.__stage1_helper_is_leaf(ast_node.get_else_exp()):
                return self.__stage1_call_in_exp(ast_node.get_else_exp())

        return False

    # Generate Intermediate code.
    #
    # The intermediate code is structured like this.
    # (operation, left, right, target)
    #
    # The argument operation is the only execption
    # (arg, function_name, input_value, argument_index)
    def generate_stage1(
        self, target_register=None, definition_name=None, ast_node=None
    ):
        # Walk the AST and generate_stage1 code
        if not ast_node:
            self.generate_stage1(ast_node=self.ast)

        if type(ast_node) == ast_nodes.Definition_List_Node:
            for definition in ast_node.get_definitions():
                if (
                    definition.get_name().get_value()
                    in self.symbol_table.get_used_funcs()
                ):
                    self.intermediate[definition.get_name().get_value()] = []
                    self.generate_stage1(
                        definition_name=definition.get_name().get_value(),
                        ast_node=definition,
                    )
            return

        if type(ast_node) == ast_nodes.Definition_Node:
            self.intermediate[definition_name].append(("setup_dmem", None, None, None))
            return_value = self.generate_stage1(
                definition_name=ast_node.get_name().get_value(),
                ast_node=ast_node.get_body(),
            )

            self.intermediate[definition_name].append(
                ("return", None, None, return_value)
            )
            return

        if type(ast_node) == ast_nodes.Body_Node:
            return_value = None
            for expression in ast_node.get_expressions():
                return_value = self.generate_stage1(
                    target_register=self.registers.get_register(),
                    definition_name=definition_name,
                    ast_node=expression,
                )
            return return_value

        if type(ast_node) == ast_nodes.Function_Call_Node:
            arg_list = None
            # Take into account single argument functions.
            if type(ast_node.get_arg_list()) == ast_nodes.Argument_List_Node:
                arg_list = ast_node.get_arg_list().get_args()
            else:
                arg_list = [ast_node.get_arg_list()]

            # Inner function calls NEED to go first.
            inner_function_calls = []
            argument_process_order = []
            for index in range(0, len(arg_list)):
                if self.__stage1_call_in_exp(arg_list[index]):
                    inner_function_calls.append(index)
                else:
                    argument_process_order.append(index)

            if len(inner_function_calls) == 1:
                argument_process_order = inner_function_calls + argument_process_order

            # The results from two or more inner function calls needs to be stored
            # in temp_variables to prevent arguments from getting overwritten.
            if len(inner_function_calls) > 1:
                temp_variables = {}
                for index in inner_function_calls:
                    argument = arg_list[index]

                    target_location = self.stackframes[definition_name].create_temp()
                    
                    return_value = self.generate_stage1(
                        target_register=target_location,
                        definition_name=definition_name,
                        ast_node=argument,
                    )
                    
                    temp_variables[index] = return_value

                for index in inner_function_calls:
                    self.intermediate[definition_name].append(
                        (
                            "arg",
                            ast_node.get_name().get_value(),
                            temp_variables[index],
                            index
                        )
                    )

            for index in argument_process_order:
                argument = arg_list[index]
                # Generate code for this argument's expression.
                return_value = self.generate_stage1(
                    target_register=self.registers.get_register(),
                    definition_name=definition_name,
                    ast_node=argument,
                )

                self.intermediate[definition_name].append(
                    (
                        "arg",
                        ast_node.get_name().get_value(),
                        return_value,
                        index,
                    )
                )

            self.intermediate[definition_name].append(
                (
                    "func_call",
                    None,
                    target_register,
                    ast_node.get_name().get_value(),
                )
            )

            return target_register

        math_operations_map = {
            ast_nodes.Add_Node: "add",
            ast_nodes.Subtract_Node: "subtract",
            ast_nodes.Multiply_Node: "multiply",
            ast_nodes.Divide_Node: "divide",
        }

        if type(ast_node) in math_operations_map.keys():
            # Don't create temp variables when it's not needed.
            left_target = None
            if not self.__stage1_helper_is_leaf(ast_node.get_term_a()):
                left_target = self.stackframes[definition_name].create_temp()
            right_target = None
            if not self.__stage1_helper_is_leaf(ast_node.get_term_b()):
                right_target = self.stackframes[definition_name].create_temp()

            # Process left and right side of expression.
            left = self.generate_stage1(
                target_register=left_target,
                definition_name=definition_name,
                ast_node=ast_node.get_term_a(),
            )
            right = self.generate_stage1(
                target_register=right_target,
                definition_name=definition_name,
                ast_node=ast_node.get_term_b(),
            )

            # Insert intermediate code for Add, Subtract, Multiply, or Divide.
            self.intermediate[definition_name].append(
                (
                    math_operations_map[type(ast_node)],
                    left,
                    right,
                    target_register,
                )
            )

            return target_register

        comparison_map = {
            ast_nodes.Equals_Node: "equals",
            ast_nodes.Less_Than_Node: "less_than",
        }

        if type(ast_node) in comparison_map.keys():
            # Don't create temp variables when it's not needed.
            left_target = None
            if not self.__stage1_helper_is_leaf(ast_node.get_exp_a()):
                left_target = self.stackframes[definition_name].create_temp()
            right_target = None
            if not self.__stage1_helper_is_leaf(ast_node.get_exp_b()):
                right_target = self.stackframes[definition_name].create_temp()

            # Process left and right side of expression.
            left = self.generate_stage1(
                target_register=left_target,
                definition_name=definition_name,
                ast_node=ast_node.get_exp_a(),
            )
            right = self.generate_stage1(
                target_register=right_target,
                definition_name=definition_name,
                ast_node=ast_node.get_exp_b(),
            )

            # If equal the unused register should be zero.
            # Store the resulting boolean into target_register.
            self.intermediate[definition_name].append(
                (
                    comparison_map[type(ast_node)],
                    left,
                    right,
                    target_register,
                )
            )

            return target_register

        bool_comparison_map = {
            ast_nodes.And_Node: "and",
            ast_nodes.Or_Node: "or",
        }

        if type(ast_node) in bool_comparison_map.keys():
            # Don't create temp variables when it's not needed.
            left_target = None
            if not self.__stage1_helper_is_leaf(ast_node.get_term_a()):
                left_target = self.stackframes[definition_name].create_temp()
            right_target = None
            if not self.__stage1_helper_is_leaf(ast_node.get_term_b()):
                right_target = self.stackframes[definition_name].create_temp()

            # Process left and right side of expression.
            left = self.generate_stage1(
                target_register=left_target,
                definition_name=definition_name,
                ast_node=ast_node.get_term_a(),
            )
            right = self.generate_stage1(
                target_register=right_target,
                definition_name=definition_name,
                ast_node=ast_node.get_term_b(),
            )

            # If equal the unused register should be zero.
            # Store the resulting boolean into target_register.
            self.intermediate[definition_name].append(
                (
                    bool_comparison_map[type(ast_node)],
                    left,
                    right,
                    target_register,
                )
            )

            return target_register

        if type(ast_node) == ast_nodes.Not_Node:
            return_value = self.generate_stage1(
                target_register=target_register,
                definition_name=definition_name,
                ast_node=ast_node.get_factor(),
            )

            self.intermediate[definition_name].append(
                (
                    "not",
                    return_value,
                    None,
                    target_register,
                )
            )
            return target_register

        if type(ast_node) == ast_nodes.If_Node:
            # We need the result to be stored in the same target
            # location, so I'll setup a temp variable.
            target_location = self.stackframes[definition_name].create_temp()

            if_exp_result = self.generate_stage1(
                target_register=target_register,
                definition_name=definition_name,
                ast_node=ast_node.get_if_exp(),
            )

            # Create label for if_then
            if_then_label = self.__stage1_helper_jump_label()
            else_label = self.__stage1_helper_jump_label()

            # Insert if instruction.
            self.intermediate[definition_name].append(
                (
                    "if_then",
                    if_exp_result,
                    None,
                    if_then_label,
                    else_label,
                )
            )

            # Process then
            if_then_result = self.generate_stage1(
                target_register=target_location,
                definition_name=definition_name,
                ast_node=ast_node.get_then_exp(),
            )

            # Label for else to end_if
            end_if_label = self.__stage1_helper_jump_label()

            # Insert else instruction.
            self.intermediate[definition_name].append(
                (
                    "else",
                    if_then_result,
                    target_location,
                    else_label,
                    end_if_label,
                )
            )

            # Process else instruction
            else_result = self.generate_stage1(
                target_register=target_location,
                definition_name=definition_name,
                ast_node=ast_node.get_else_exp(),
            )

            # Insert end_if instruction
            self.intermediate[definition_name].append(
                (
                    "end_if",
                    else_result,
                    target_location,
                    None,
                    end_if_label,
                )
            )

            return target_location

        if type(ast_node) == ast_nodes.Negative_Node:
            return_value = self.generate_stage1(
                target_register=target_register,
                definition_name=definition_name,
                ast_node=ast_node.get_factor(),
            )

            self.intermediate[definition_name].append(
                (
                    "negative",
                    return_value,
                    self.registers.get_register(),
                    target_register,
                )
            )
            return target_register

        if type(ast_node) == ast_nodes.Identifier_Node:
            # Simply return the ast_node the helper function
            # will handle the rest.
            return ast_node

        if type(ast_node) == ast_nodes.Integer_Lit_Node:
            # Simply return the ast_node the helper function
            # will handle the rest.
            return ast_node

        if type(ast_node) == ast_nodes.Boolean_Lit_Node:
            # Simply return the ast_node the helper function
            # will handle the rest.
            return ast_node

    def generate_stage2(self):
        # Take stage1 code and translate into TM.
        intermediate_stage2 = {}

        # Get comments from intermediate code.
        comments = self.stage1_code_to_comments()

        # Print implementation.
        print_tm = [
            ("LD", 1, -3, 6, "print arg #0 to r1"),
            ("OUT", 1, 0, 0, "Print value of r1"),
            ("ST", 1, -2, 6, "r1 to print's return value"),
            ("LD", 5, -1, 6, "print's return addr to r5"),
            ("LDA", 6, -3, 6, "Deallocate print's stack frame"),
            ("LDA", 7, 0, 5, "Branch to return address"),
        ]
        intermediate_stage2["print"] = print_tm

        for definition in self.intermediate.keys():
            intermediate_stage2[definition] = []
            self.code_comments[definition] = {}

            stage1_code = self.intermediate[definition]
            stage1_comments = comments[definition]

            for index in range(0, len(stage1_code)):
                # We'll need statement to be a list for
                # pass by reference to work.
                statement = list(stage1_code[index])

                # Insert comment showing what a chunk of TM code is doing.
                if index > 0:
                    self.code_comments[definition][
                        len(intermediate_stage2[definition])
                    ] = stage1_comments[index]

                if statement[0] == "setup_dmem":
                    intermediate_stage2[definition] += code_emitters.setup_dmem_to_tm(
                        self.stackframes[definition]
                    )

                if statement[0] == "arg":
                    # Load variables
                    input_value = self.__stage2_helper_load_variable(
                        definition, statement, 2
                    )
                    intermediate_stage2[definition] += input_value
                    intermediate_stage2[definition] += code_emitters.arg_to_tm(
                        self.stackframes[definition], tuple(statement)
                    )

                if statement[0] == "func_call":
                    self.stackframes[statement[3]].set_called_stack_frame(
                        self.stackframes[definition]
                    )
                    # Setup temp variables if needed.
                    temp_var = statement[2]
                    self.__stage2_helper_temp_store_register(statement, 2)
                    intermediate_stage2[definition] += code_emitters.func_call_to_tm(
                        self.stackframes[statement[3]],
                        self.stackframes[definition],
                        tuple(statement),
                        statement[3],
                    )
                    # Store results into temp variable if needed.
                    intermediate_stage2[definition] += self.__stage2_helper_temp_dmem(
                        definition, temp_var
                    )

                if statement[0] in ["add", "subtract", "multiply", "divide"]:
                    # Load variables if needed
                    left = self.__stage2_helper_load_variable(definition, statement, 1)
                    intermediate_stage2[definition] += left
                    right = self.__stage2_helper_load_variable(definition, statement, 2)
                    intermediate_stage2[definition] += right
                    # Setup temp variables if needed.
                    temp_var = statement[3]
                    self.__stage2_helper_temp_store_register(statement, 3)
                    intermediate_stage2[definition] += code_emitters.math_to_tm(
                        tuple(statement)
                    )
                    # Store results into temp variable if needed.
                    intermediate_stage2[definition] += self.__stage2_helper_temp_dmem(
                        definition, temp_var
                    )

                if statement[0] in ["equals", "less_than", "and", "or"]:
                    # Load variables if needed
                    left = self.__stage2_helper_load_variable(definition, statement, 1)
                    intermediate_stage2[definition] += left
                    right = self.__stage2_helper_load_variable(definition, statement, 2)
                    intermediate_stage2[definition] += right
                    # Setup temp variables if needed.
                    temp_var = statement[3]
                    self.__stage2_helper_temp_store_register(statement, 3)
                    intermediate_stage2[definition] += code_emitters.comparison_to_tm(
                        tuple(statement)
                    )
                    # Store results into temp variable if needed.
                    intermediate_stage2[definition] += self.__stage2_helper_temp_dmem(
                        definition, temp_var
                    )

                if statement[0] == "if_then":
                    # Load if_then's results.
                    if_then_exp = self.__stage2_helper_load_variable(
                        definition, statement, 1
                    )
                    intermediate_stage2[definition] += if_then_exp
                    intermediate_stage2[definition] += code_emitters.if_then_to_tm(
                        tuple(statement)
                    )

                if statement[0] == "else":
                    # Check if input and output are the same.
                    # to prevent un-needed loads and stores.
                    if statement[1] != statement[2]:
                        # Load if_then's results.
                        if_then_results = self.__stage2_helper_load_variable(
                            definition, statement, 1
                        )
                        intermediate_stage2[definition] += if_then_results
                        # Store if_then's results.
                        temp_var = statement[2]
                        self.__stage2_helper_temp_store_register(statement, 2)
                        # Store results into temp variable if needed.
                        intermediate_stage2[
                            definition
                        ] += self.__stage2_helper_temp_dmem(
                            definition, temp_var, statement[1]
                        )
                    intermediate_stage2[definition] += code_emitters.else_to_tm(
                        tuple(statement)
                    )

                if statement[0] == "end_if":
                    # Check if input and output are the same.
                    # to prevent un-needed loads and stores.
                    if statement[1] != statement[2]:
                        # Load else's results.
                        end_if_results = self.__stage2_helper_load_variable(
                            definition, statement, 1
                        )
                        intermediate_stage2[definition] += end_if_results
                        # Store else's results.
                        temp_var = statement[2]
                        self.__stage2_helper_temp_store_register(statement, 2)
                        # Store results into temp variable if needed.
                        intermediate_stage2[
                            definition
                        ] += self.__stage2_helper_temp_dmem(
                            definition, temp_var, statement[1]
                        )
                    intermediate_stage2[definition] += code_emitters.end_if_to_tm(
                        tuple(statement)
                    )

                if statement[0] == "not":
                    # Load variables
                    input_value = self.__stage2_helper_load_variable(
                        definition, statement, 1
                    )
                    intermediate_stage2[definition] += input_value
                    # Setup temp variables if needed.
                    temp_var = statement[3]
                    self.__stage2_helper_temp_store_register(statement, 3)
                    intermediate_stage2[definition] += code_emitters.not_to_tm(
                        tuple(statement)
                    )
                    # Store results into temp variable if needed.
                    intermediate_stage2[definition] += self.__stage2_helper_temp_dmem(
                        definition, temp_var
                    )

                if statement[0] == "negative":
                    # Load variables
                    input_value = self.__stage2_helper_load_variable(
                        definition, statement, 1
                    )
                    intermediate_stage2[definition] += input_value
                    # Setup temp variables if needed.
                    temp_var = statement[3]
                    self.__stage2_helper_temp_store_register(statement, 3)
                    intermediate_stage2[definition] += code_emitters.negative_to_tm(
                        tuple(statement)
                    )
                    # Store results into temp variable if needed.
                    intermediate_stage2[definition] += self.__stage2_helper_temp_dmem(
                        definition, temp_var
                    )

                if statement[0] == "return":
                    return_value = self.__stage2_helper_load_variable(
                        definition, statement, 3
                    )
                    intermediate_stage2[definition] += return_value
                    intermediate_stage2[definition] += code_emitters.return_to_tm(
                        self.stackframes[definition], definition, tuple(statement)
                    )

        self.intermediate = intermediate_stage2

        # Replace jump labels.
        for definition in self.intermediate.keys():
            code = self.intermediate[definition]
            # code = [x for x in code if code[0] != "STAGE1_COMMENT"]

            # Convert all code to list.
            for index in range(0, len(code)):
                code[index] = list(code[index])
                if len(code[index]) == 5:
                    code[index].append(None)

            # Find all "from" jump labels.
            from_jump_labels = {}
            for index in range(0, len(code)):
                if "JUMP_LABEL_" in str(code[index][2]):
                    from_jump_labels[code[index][2]] = index

            # Find all "to" jump labels.
            to_jump_labels = {}
            for index in range(0, len(code)):
                if "JUMP_LABEL_" in str(code[index][5]):
                    to_jump_labels[code[index][5]] = index

            # Replace all jump labels with their proper locations.
            for label in from_jump_labels:
                jump_size = to_jump_labels[label] - from_jump_labels[label]
                code[from_jump_labels[label]][2] = jump_size

            # Convert all code back to tuple.
            for index in range(0, len(code)):
                # code[index].pop()
                code[index] = tuple(code[index])

            self.intermediate[definition] = code

    # Create final TM code.
    def generate_final(self):
        registers = Registers()
        function_locations = {}

        main_stack_frame = None
        if self.symbol_table:
            main_stack_frame = StackFrame(self.symbol_table.get_entry("main"))

        if not main_stack_frame:
            return

        # Setup init code
        code = [
            (
                "LDA",
                registers.return_addr,
                3,
                registers.program_counter,
                "Put return address in registor {}".format(registers.return_addr),
            ),
            (
                "ST",
                registers.return_addr,
                main_stack_frame.initial_return_addr() + 1,
                registers.dmem_top,
                "Store return address in {}'s stack frame at top+{}".format(
                    "main", main_stack_frame.initial_return_addr()
                ),
            ),
            (
                "LDA",
                registers.dmem_top,
                main_stack_frame.initial_return_addr() + 2,
                registers.dmem_top,
                "Update top to {}'s top of stack frame".format("main"),
            ),
            (
                "LDA",
                registers.program_counter,
                "main",
                registers.zero,
                "Branch to {}".format("main"),
            ),
            (
                "LD",
                1,
                self.stackframes["main"].initial_return_addr() - 1,
                registers.dmem_top,
                "r1 = 'main' return value",
            ),
            (
                "OUT",
                1,
                self.registers.zero,
                self.registers.zero,
                "Print 'main's return value.",
            ),
            ("HALT", registers.zero, registers.zero, registers.zero, "Exit program"),
        ]

        for definition in self.intermediate.keys():
            function_locations[definition] = len(code)
            code += self.intermediate[definition]

        # Fix function call IMEM locations.
        for index in range(0, len(code)):
            for definition in self.intermediate.keys():
                if code[index][2] == definition and code[index][0] == "LDA":
                    code_instruction = list(code[index])
                    code_instruction[2] = function_locations[definition]
                    code[index] = tuple(code_instruction)

        location_to_function = {}
        for key in function_locations.keys():
            location_to_function[function_locations[key]] = key

        ro_opcodes = ["IN", "OUT", "ADD", "SUB", "MUL", "DIV", "HALT"]

        return_string = "*\n"
        return_string += "* Initial code\n"
        return_string += "*\n"
        current_function = "init"
        comment_counter = 0
        for index in range(0, len(code)):
            if index in location_to_function.keys():
                current_function = location_to_function[index]
                # Format functions like so: 'foo(a, b, c)'
                function_string = "{}(".format(location_to_function[index])
                param_list = self.symbol_table.get_entry(
                    current_function
                ).get_param_list()
                for arg in param_list:
                    function_string += "{}".format(arg)
                    if arg != param_list[-1]:
                        function_string += ", "
                function_string += ")"
                return_string += "\n"
                return_string += "*\n"
                return_string += "* def {}\n".format(function_string)
                return_string += "*\n"
                comment_counter = 0

            if current_function in self.code_comments.keys():
                if comment_counter in self.code_comments[current_function].keys():
                    return_string += "*\n"
                    return_string += "* Intermediate: {}\n".format(
                        self.code_comments[current_function][comment_counter].lstrip()
                    )
                    return_string += "*\n"

            if code[index][0] in ro_opcodes:
                without_comment = "{0: >3}: {1: >3} {2:},{3:},{4:}".format(
                    index,
                    code[index][0],
                    code[index][1],
                    code[index][2],
                    code[index][3],
                )
                return_string += "{0: <20}; {1:}\n".format(
                    without_comment, code[index][4]
                )
                comment_counter += 1
            else:
                without_comment = "{0: >3}: {1: >3} {2:},{3:}({4:})".format(
                    index,
                    code[index][0],
                    code[index][1],
                    code[index][2],
                    code[index][3],
                )
                return_string += "{0: <20}; {1:}\n".format(
                    without_comment, code[index][4]
                )
                comment_counter += 1

        return return_string
