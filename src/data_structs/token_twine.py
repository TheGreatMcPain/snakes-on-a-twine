from enum import Enum


class TokenType(Enum):
    id_token = 0
    id_print = 1
    keyword_f = 2
    keyword_returns = 3
    keyword_if = 6
    keyword_else = 7
    typename_boolean = 8
    typename_integer = 25
    int_token = 9
    bool_token = 10
    op_equals = 11
    op_plus = 12
    op_minus = 13
    op_multiply = 14
    op_divide = 15
    op_lessthan = 16
    op_and = 17
    op_or = 18
    op_not = 19
    punct_leftparen = 20
    punct_rightparen = 21
    punct_colon = 22
    punct_comma = 23
    end_of_file = 24


class Token:
    def __init__(self, token_type, token_value, line_no, char_no, current_line):
        self.token_type = token_type
        self.token_value = token_value
        self.line_no = line_no
        self.char_no = char_no
        self.current_line = current_line
        self.filename = "Unknown"

    def setFilename(self, filename: str):
        self.filename = filename

    def getFilename(self):
        return self.filename

    def isId(self):
        return self.token_type == TokenType.id_token

    def isIdPrint(self):
        return self.token_type == TokenType.id_print

    def isKeywordF(self):
        return self.token_type == TokenType.keyword_f

    def isKeywordReturns(self):
        return self.token_type == TokenType.keyword_returns

    def isKeywordIf(self):
        return self.token_type == TokenType.keyword_if

    def isKeywordElse(self):
        return self.token_type == TokenType.keyword_else

    def isTypenameBoolean(self):
        return self.token_type == TokenType.typename_boolean

    def isTypenameInteger(self):
        return self.token_type == TokenType.typename_integer

    def isInteger(self):
        return self.token_type == TokenType.int_token

    def isBoolean(self):
        return self.token_type == TokenType.bool_token

    def isEquals(self):
        return self.token_type == TokenType.op_equals

    def isPlus(self):
        return self.token_type == TokenType.op_plus

    def isMinus(self):
        return self.token_type == TokenType.op_minus

    def isMultiply(self):
        return self.token_type == TokenType.op_multiply

    def isDivide(self):
        return self.token_type == TokenType.op_divide

    def isLessThan(self):
        return self.token_type == TokenType.op_lessthan

    def isAnd(self):
        return self.token_type == TokenType.op_and

    def isOr(self):
        return self.token_type == TokenType.op_or

    def isNot(self):
        return self.token_type == TokenType.op_not

    def isLeftParen(self):
        return self.token_type == TokenType.punct_leftparen

    def isRightParen(self):
        return self.token_type == TokenType.punct_rightparen

    def isColon(self):
        return self.token_type == TokenType.punct_colon

    def isComma(self):
        return self.token_type == TokenType.punct_comma

    def isEof(self):
        return self.token_type == TokenType.end_of_file

    def value(self):
        return self.token_value

    def lineNo(self):
        return self.line_no

    def __repr__(self):
        if self.isId() or self.isIdPrint():
            return 'identifier = ' + self.token_value

        elif self.isKeywordF() or self.isKeywordReturns(
        ) or self.isKeywordIf() or self.isKeywordElse():
            return 'keyword = ' + self.token_value

        elif self.isTypenameBoolean() or self.isTypenameInteger():
            return 'typename = ' + self.token_value

        elif self.isInteger():
            return 'integer = ' + str(self.token_value)

        elif self.isBoolean():
            return 'boolean = ' + self.token_value

        elif self.isEquals():
            return 'operator equals'

        elif self.isPlus():
            return 'operator plus'

        elif self.isMinus():
            return 'operator minus'

        elif self.isMultiply():
            return 'operator multiply'

        elif self.isDivide():
            return 'operator divide'

        elif self.isLessThan():
            return 'operator lessthan'

        elif self.isAnd():
            return 'operator and'

        elif self.isOr():
            return 'operator or'

        elif self.isNot():
            return 'operator not'

        elif self.isLeftParen():
            return 'punctuation leftparen'

        elif self.isRightParen():
            return 'punctuation rightparen'

        elif self.isColon():
            return 'punctuation colon'

        elif self.isComma():
            return 'punctuation comma'

        elif self.isEof():
            return 'end_of_file'
