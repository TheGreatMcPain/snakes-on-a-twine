from src.data_structs.ast_nodes import Int_Type, Param_Node, Name_Node


class Symbol_Table_Entry:
    def __init__(self, token, data_type, params):
        self.token = token
        self.data_type = data_type
        self.params = {}
        for param in params:
            self.params[param.get_name().get_value()] = param.get_type()
        # There's a small change that the self.params dictionary keys won't be
        # in order, so we'll also store the param names in a list.
        self.param_list = [x.get_name().get_value() for x in params]
        self.is_duplicate = False
        self.func_calls = []
        self.calling_funcs = []
        self.unused_params = [param.get_name().get_value() for param in params]

    def get_token(self):
        return self.token

    def get_type(self):
        return self.data_type

    def set_type(self, type):
        self.data_type = type

    def get_params(self):
        return self.params

    def is_param(self, param):
        return param in self.params

    def get_param_list(self):
        return self.param_list

    def get_param_index(self, param):
        return self.param_list.index(param)

    def get_param_type(self, param):
        return self.params[param]

    def get_unused_params(self):
        return self.unused_params

    def is_unused_param(self, param):
        return param in self.unused_params

    def del_unused_param(self, param):
        self.unused_params.remove(param)

    def get_func_calls(self):
        return self.func_calls

    def is_func_call(self, function_name):
        return function_name in self.func_calls

    def add_func_call(self, function_name):
        self.func_calls.append(function_name)

    def get_calling_funcs(self):
        return self.calling_funcs

    def is_calling_func(self, function_name):
        return function_name in self.calling_funcs

    def add_calling_func(self, function_name):
        self.calling_funcs.append(function_name)

    def mark_duplicate(self):
        self.is_duplicate = True

    def __str__(self):
        printable = ""
        if self.is_duplicate:
            printable += "\tDuplicate Function Names!\n"
        printable += "\tType: " + str(self.data_type)
        printable += "\n\tParameters: "
        for param in self.params:
            printable += "\n\t\tname: " + param + "; type: " + str(self.params[param])
        printable += "\n\tFunctions Called By This: "
        for func in self.func_calls:
            printable += "\n\t\tname: " + func
        printable += "\n\tFunctions That Call This:"
        for func in self.calling_funcs:
            printable += "\n\t\tname: " + func

        return printable


class Symbol_Table:
    def __init__(self):
        self.symbol_table = {}
        self.dup_func_names = []  # List of functions that share the same name.

        # Add print to symbol_table
        print_param = Param_Node(None, Name_Node(None, "EXPRESSION"), None)
        print_param.type = Int_Type()
        self.add_entry(None, "print", Int_Type(), [print_param])

    def get_table(self):
        return self.symbol_table

    def is_entry(self, function_name):
        return function_name in self.symbol_table

    def get_entry(self, function_name):
        return self.symbol_table[function_name]

    def add_entry(self, token, function_name, data_type, params):
        if self.is_entry(function_name):
            self.dup_func_names.append(function_name)
        self.symbol_table[function_name] = Symbol_Table_Entry(token, data_type, params)

    def get_dup_func_names(self):
        return self.dup_func_names

    def get_used_funcs(self, functions: list = [], current_function: str = ""):
        if len(functions) == 0 and current_function == "":
            functions.append("main")
            self.get_used_funcs(functions, "main")
            return functions

        if current_function == "":
            return functions

        current_entry = self.get_entry(current_function)

        for function in current_entry.get_func_calls():
            if function not in functions:
                functions.append(function)
                self.get_used_funcs(functions, function)

    def __str__(self):
        printable = ""
        for entry in self.symbol_table:
            printable += entry + "\n" + str(self.symbol_table[entry]) + "\n"

        return printable
