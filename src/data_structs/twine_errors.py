import sys
from src.data_structs.token_twine import Token

# See:
# https://gist.github.com/jhazelwo/86124774833c6ab8f973323cb9c7e251
class QuietError(Exception):
    # All who inherit me shall not traceback, but be spoken of cleanly.
    pass


# Returns string used in most error messages.
def error_location_info(filename, line_of_error, char_of_error, line_contents):
    msg = (
        "'{}'{}:{}\n".format(filename, line_of_error, char_of_error)
        + "    |\n"
        + "    |"
        + line_contents
        + "\n"
        + "    |"
        + " " * char_of_error
        + "^\n"
    )
    return msg


# Raised when the scanner encounters a lexical issue in the input program
class TwineLexicalError(QuietError):
    # Removes the leading 'src' name
    __module__ = __module__.replace("src", "")

    # Remove any leading '.' characters
    while __module__[0] == ".":
        __module__ = __module__[1:]

    def __init__(
        self,
        filename,
        line_of_error,
        char_of_error,
        line_contents,
        scanner_state,
        error_msg,
    ):
        self.filename = filename
        self.line_of_error = str(line_of_error)
        self.char_of_error = char_of_error
        self.line_contents = line_contents
        self.scanner_state = scanner_state
        self.error_msg = error_msg

    def __str__(self):
        msg = (
            "While scanner in state ["
            + self.scanner_state
            + "] "
            + error_location_info(
                self.filename,
                self.line_of_error,
                self.char_of_error,
                self.line_contents,
            )
            + "    "
            + self.error_msg
        )

        return msg


# Errors thrown by the parser
class TwineSyntacticError(QuietError):
    # Removes the leading 'src' name
    __module__ = __module__.replace("src", "")

    # Remove any leading '.' characters
    while __module__[0] == ".":
        __module__ = __module__[1:]

    def __init__(self, token: Token, error_msg):
        self.token = token
        self.error_msg = error_msg

    def __str__(self):
        msg = (
            error_location_info(
                self.token.getFilename(),
                self.token.lineNo(),
                self.token.char_no,
                self.token.current_line,
            )
            + "    "
            + self.error_msg
        )

        return msg


class TwineEofError(QuietError):
    __module__ = __module__.replace("src", "")

    while __module__[0] == ".":
        __module__ = __module__[1:]

    def __init__(self, error_msg):
        self.error_msg = error_msg

    def __str__(self):
        return self.error_msg


# Errors thrown by the type checker
class TwineSemanticError(QuietError):
    __module__ = __module__.replace("src", "")

    while __module__[0] == ".":
        __module__ = __module__[1:]

    def __init__(self, error_msg):
        self.error_msg = error_msg

    def __str__(self):
        return self.error_msg


# Errors thrown by Code Generator
class TwineCodeGenError(QuietError):
    __module__ = __module__.replace("src", "")

    while __module__[0] == ".":
        __module__ = __module__[1:]

    def __init__(self, error_msg):
        self.error_msg = error_msg

    def __str__(self):
        return self.error_msg


# See:
# https://gist.github.com/jhazelwo/86124774833c6ab8f973323cb9c7e251
def quiet_hook(kind, message, traceback):
    if QuietError in kind.__bases__:
        # Only print Error Type and Message
        print("{0}: {1}".format(kind.__name__, message))
    else:
        # Print Error Type, Message, and Traceback
        sys.__excepthook__(kind, message, traceback)


sys.excepthook = quiet_hook
