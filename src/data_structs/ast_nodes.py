from src.data_structs.token_twine import Token
from src.data_structs.twine_errors import error_location_info

"""
Types for AST nodes (used for semantic analysis)
"""
# Root class
class AST_Type(object):
    pass


# Concrete classes
class Int_Type(AST_Type):
    def __str__(self):
        printable = "INTEGER"
        return printable


class Boolean_Type(AST_Type):
    def __str__(self):
        printable = "BOOLEAN"
        return printable


class Function_Type(AST_Type):
    def __init__(self, param_list, return_type):
        self.param_types = [param.get_type() for param in param_list.get_params()]
        self.return_type = return_type

    def get_param_types(self):
        return self.param_types

    def get_return_type(self):
        return self.return_type

    def __str__(self):
        printable = "FUNCTION"
        return printable


class None_Type(AST_Type):
    def __str__(self):
        printable = "NONE"
        return printable


class Error_Type(AST_Type):
    # Leaving 'token' with 'None' will simply return the input error message.
    # Adding a token will also include the filename, line number, column, and line contents.
    def __init__(
        self, token: Token = None, error_msg="Semantic error detected in program"
    ):
        self.error_msg = error_msg
        self.token = token

    def get_msg(self):
        if self.token:
            return (
                error_location_info(
                    self.token.getFilename(),
                    self.token.lineNo(),
                    self.token.char_no,
                    self.token.current_line,
                )
                + self.error_msg
            )
        else:
            return self.error_msg

    def __str__(self):
        printable = "ERROR"
        return printable


"""
AST Nodes
"""
# Root class
class AST_Node(object):
    def __init__(self):
        self.type = None_Type()

    def get_type(self):
        return self.type


# Concrete classes
class Definition_List_Node(AST_Node):
    def __init__(self, token, definitions):
        self.token = token
        self.definitions = definitions

    def get_token(self):
        return self.token

    def get_definitions(self):
        return self.definitions

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "definition-list\n"
        for definition in self.definitions:
            tree += definition.__str__(num_tabs + 1) + "\n"
        return tree[:-1]

    def set_type(self, symbol_table, current_func):
        self.type = None_Type()
        # Initialize symbol_table
        for definition in self.definitions:
            definition.init_symbol_table(symbol_table)

        # Analyze AST and append type information.
        error_msg = ""
        visited = []
        for definition in self.definitions:
            if definition.get_name().get_value() not in visited:
                definition.set_type(symbol_table, current_func)
                visited.append(definition.get_name().get_value())
                if type(definition.get_type()) == Error_Type:
                    error_msg += definition.get_type().get_msg()
            if len(error_msg) > 0:
                self.type = Error_Type(None, error_msg)

        # Check if symbol_table has duplicate function names.
        if len(symbol_table.get_dup_func_names()) > 0:
            for func in symbol_table.get_dup_func_names():
                symbol_table.get_entry(func).mark_duplicate()
                error_msg += "\n    Duplicate function: '" + func + "'"

            self.type = Error_Type(None, error_msg)

        total_warning_msg = ""
        for func_name in symbol_table.get_table():
            entry = symbol_table.get_entry(func_name)
            warning_msg = ""
            if len(entry.get_calling_funcs()) == 0 and func_name not in (
                "print",
                "main",
            ):
                warning_msg = (
                    "\nWarnings in '"
                    + func_name
                    + "':"
                    + " Line "
                    + str(entry.get_token().line_no)
                    + ", Column "
                    + str(entry.get_token().char_no)
                )
                warning_msg += "\n    Function is unused"

            if len(entry.get_unused_params()) > 0 and func_name not in ("print"):
                if warning_msg == "":
                    warning_msg = (
                        "\nWarnings in '"
                        + func_name
                        + "':"
                        + " Line "
                        + str(entry.get_token().line_no)
                        + ", Column "
                        + str(entry.get_token().char_no)
                    )
                warning_msg += "\n    Unused parameters: "
                for unused_param in entry.get_unused_params():
                    warning_msg += unused_param + ", "

            if warning_msg != "":
                warning_msg += "\n"
                total_warning_msg += warning_msg

        if total_warning_msg != "":
            print(total_warning_msg + "\n")


class Definition_Node(AST_Node):
    def __init__(self, token, name, param_list, func_return, body):
        self.token = token
        self.name = name
        self.param_list = param_list
        self.func_return = func_return
        self.body = body

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_name(self):
        return self.name

    def get_param_list(self):
        return self.param_list

    def get_func_return(self):
        return self.func_return

    def get_body(self):
        return self.body

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "definition\n"
        tree += self.name.__str__(num_tabs + 1) + "\n"
        tree += self.param_list.__str__(num_tabs + 1) + "\n"
        tree += self.func_return.__str__(num_tabs + 1) + "\n"
        tree += self.body.__str__(num_tabs + 1)
        return tree

    def init_symbol_table(self, symbol_table):
        self.name.set_type(symbol_table, self.name.get_value())
        self.func_return.set_type(symbol_table, self.name.get_value())
        self.param_list.set_type(symbol_table, self.name.get_value())

        self.type = Function_Type(self.param_list, self.func_return.get_type())

        symbol_table.add_entry(
            self.get_token(),
            self.name.get_value(),
            self.type.get_return_type(),
            self.param_list.get_params(),
        )

    def set_type(self, symbol_table, current_func):
        error_msg = ""
        # Check for duplicate parameter names.
        param_names = [x.get_name().get_value() for x in self.param_list.get_params()]
        error_found = False
        for x in range(0, len(param_names)):
            for y in range(0, len(param_names)):
                if y != x and param_names[x] == param_names[y]:
                    if not error_found:
                        error_msg += (
                            "\n    Duplicate parameter name: '"
                            + param_names[x]
                            + "' in function: '"
                            + self.name.get_value()
                            + "'"
                        )
                        error_found = True
                    self.type = Error_Type(None, error_msg)
                    symbol_table.get_entry(self.name.get_value()).set_type(self.type)
        self.body.set_type(symbol_table, self.name.get_value())

        # Change Type to Error and update symbol table if the function's return
        # type doesn't match what the body returns.
        if not type(self.body.get_type()) == type(self.func_return.get_type()):
            if type(self.body.get_type()) == Error_Type:
                error_msg += (
                    "\n\nError in '"
                    + self.name.get_value()
                    + "' in file "
                    + self.body.get_type().get_msg()
                )
            else:
                error_msg += (
                    "\n    Function: '"
                    + self.name.get_value()
                    + "' returns wrong type"
                    + "\n        Expected: "
                    + str(self.func_return.get_type())
                    + ", Returned: "
                    + str(self.body.get_type())
                )
            self.type = Error_Type(None, error_msg)
            symbol_table_entry = symbol_table.get_entry(self.name.get_value())
            symbol_table_entry.set_type(self.type)


class Identifier_Node(AST_Node):
    def __init__(self, token, value):
        self.token = token
        self.value = value

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_value(self):
        return self.value

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "id\t" + self.value
        return tree

    def set_type(self, symbol_table, current_func):
        symbol_table_entry = symbol_table.get_entry(current_func)

        if symbol_table_entry.is_param(self.value):
            # Since we know the parameter get's used remove it
            # from the unused parameter list.
            if symbol_table_entry.is_unused_param(self.value):
                symbol_table_entry.del_unused_param(self.value)
            self.type = symbol_table_entry.get_param_type(self.value)
            return

        error_msg = "    Unknown identifier: " + self.value
        self.type = Error_Type(None, error_msg)


class Parameter_List_Node(AST_Node):
    def __init__(self, token, params):
        self.token = token
        self.params = params

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_params(self):
        return self.params

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "param-list\n"
        if len(self.params) == 0:
            tree += "\t" * (num_tabs + 1) + "none"
        else:
            for param in self.params:
                tree += param.__str__(num_tabs + 1) + "\n"
        return tree

    def set_type(self, symbol_table, current_func):
        for param in self.get_params():
            param.set_type(symbol_table, current_func)


class Function_Return_Node(AST_Node):
    def __init__(self, token, return_type):
        self.token = token
        self.return_type = return_type

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_return_type(self):
        return self.return_type

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "func-return\n"
        tree += self.return_type.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        self.return_type.set_type(symbol_table, current_func)
        self.type = self.return_type.get_type()


class Body_Node(AST_Node):
    def __init__(self, token, expressions):
        self.token = token
        self.expressions = expressions

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_expressions(self):
        return self.expressions

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "body\n"
        for exp in self.expressions:
            tree += exp.__str__(num_tabs + 1) + "\n"
        return tree[:-1]

    def set_type(self, symbol_table, current_func):
        for expression in self.expressions:
            expression.set_type(symbol_table, current_func)
        self.type = self.expressions[-1].get_type()


class Param_Node(AST_Node):
    def __init__(self, token, name, param_type):
        self.token = token
        self.name = name
        self.param_type = param_type

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_name(self):
        return self.name

    def get_param_type(self):
        return self.param_type

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "param\n"
        tree += self.name.__str__(num_tabs + 1) + "\n"
        tree += self.param_type.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        self.param_type.set_type(symbol_table, current_func)
        self.type = self.param_type.get_type()


class Type_Node(AST_Node):
    def __init__(self, token, value):
        self.token = token
        self.value = value

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_value(self):
        return self.value

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "type\t" + self.value
        return tree

    def set_type(self, symbol_table, current_func):
        if self.value == "integer":
            self.type = Int_Type()
        else:
            self.type = Boolean_Type()


class Equals_Node(AST_Node):
    def __init__(self, token, exp_a, exp_b):
        self.token = token
        self.exp_a = exp_a
        self.exp_b = exp_b

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_exp_a(self):
        return self.exp_a

    def get_exp_b(self):
        return self.exp_b

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "equals\n"
        tree += self.exp_a.__str__(num_tabs + 1) + "\n"
        tree += self.exp_b.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        self.exp_a.set_type(symbol_table, current_func)
        self.exp_b.set_type(symbol_table, current_func)
        if type(self.exp_a.get_type()) == type(self.exp_b.get_type()):
            self.type = Boolean_Type()
        else:
            error_msg = (
                "    Can't compute equality with types:\n        "
                + str(self.exp_a.get_type())
                + " and "
                + str(self.exp_b.get_type())
            )
            self.type = Error_Type(self.get_token(), error_msg)


class Less_Than_Node(AST_Node):
    def __init__(self, token, exp_a, exp_b):
        self.token = token
        self.exp_a = exp_a
        self.exp_b = exp_b

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_exp_a(self):
        return self.exp_a

    def get_exp_b(self):
        return self.exp_b

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "less-than\n"
        tree += self.exp_a.__str__(num_tabs + 1) + "\n"
        tree += self.exp_b.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        self.exp_a.set_type(symbol_table, current_func)
        self.exp_b.set_type(symbol_table, current_func)
        if (
            type(self.exp_a.get_type()) == Int_Type
            and type(self.exp_b.get_type()) == Int_Type
        ):
            self.type = Boolean_Type()
        else:
            error_msg = (
                "    Can't compute 'less than' with types:\n        "
                + str(self.exp_a.get_type())
                + " and "
                + str(self.exp_b.get_type())
            )
            self.type = Error_Type(self.get_token(), error_msg)


class Or_Node(AST_Node):
    def __init__(self, token, term_a, term_b):
        self.token = token
        self.term_a = term_a
        self.term_b = term_b

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_term_a(self):
        return self.term_a

    def get_term_b(self):
        return self.term_b

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "or\n"
        tree += self.term_a.__str__(num_tabs + 1) + "\n"
        tree += self.term_b.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        self.term_a.set_type(symbol_table, current_func)
        self.term_b.set_type(symbol_table, current_func)
        if (
            type(self.term_a.get_type()) == Boolean_Type
            and type(self.term_b.get_type()) == Boolean_Type
        ):
            self.type = Boolean_Type()
        else:
            error_msg = (
                "    Can't compute 'or' with types:\n        "
                + str(self.term_a.get_type())
                + " and "
                + str(self.term_b.get_type())
            )
            self.type = Error_Type(self.get_token(), error_msg)


class Add_Node(AST_Node):
    def __init__(self, token, term_a, term_b):
        self.token = token
        self.term_a = term_a
        self.term_b = term_b

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_term_a(self):
        return self.term_a

    def get_term_b(self):
        return self.term_b

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "add\n"
        tree += self.term_a.__str__(num_tabs + 1) + "\n"
        tree += self.term_b.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        self.term_a.set_type(symbol_table, current_func)
        self.term_b.set_type(symbol_table, current_func)
        if (
            type(self.term_a.get_type()) == Int_Type
            and type(self.term_b.get_type()) == Int_Type
        ):
            self.type = Int_Type()
        else:
            error_msg = ""
            if (
                type(self.term_a.get_type()) == Error_Type
                or type(self.term_b.get_type()) == Error_Type
            ):
                if type(self.term_a.get_type()) == Error_Type:
                    error_msg += self.term_a.get_type().get_msg()
                if type(self.term_b.get_type()) == Error_Type:
                    if len(error_msg) > 0:
                        error_msg += "\n"
                    error_msg += self.term_b.get_type().get_msg()
            else:
                error_msg += (
                    "    Can't compute addition with types:\n        "
                    + str(self.term_a.get_type())
                    + " and "
                    + str(self.term_b.get_type())
                )
            self.type = Error_Type(self.get_token(), error_msg)


class Subtract_Node(AST_Node):
    def __init__(self, token, term_a, term_b):
        self.token = token
        self.term_a = term_a
        self.term_b = term_b

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_term_a(self):
        return self.term_a

    def get_term_b(self):
        return self.term_b

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "subtract\n"
        tree += self.term_a.__str__(num_tabs + 1) + "\n"
        tree += self.term_b.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        self.term_a.set_type(symbol_table, current_func)
        self.term_b.set_type(symbol_table, current_func)
        if (
            type(self.term_a.get_type()) == Int_Type
            and type(self.term_b.get_type()) == Int_Type
        ):
            self.type = Int_Type()
        else:
            error_msg = (
                "    Can't compute subtraction with types:\n        "
                + str(self.term_a.get_type())
                + " and "
                + str(self.term_b.get_type())
            )
            self.type = Error_Type(self.get_token(), error_msg)


class And_Node(AST_Node):
    def __init__(self, token, term_a, term_b):
        self.token = token
        self.term_a = term_a
        self.term_b = term_b

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_term_a(self):
        return self.term_a

    def get_term_b(self):
        return self.term_b

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "and\n"
        tree += self.term_a.__str__(num_tabs + 1) + "\n"
        tree += self.term_b.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        self.term_a.set_type(symbol_table, current_func)
        self.term_b.set_type(symbol_table, current_func)
        if (
            type(self.term_a.get_type()) == Boolean_Type
            and type(self.term_b.get_type()) == Boolean_Type
        ):
            self.type = Boolean_Type()
        else:
            error_msg = (
                "    Can't compute 'and' with types:\n        "
                + str(self.term_a.get_type())
                + " and "
                + str(self.term_b.get_type())
            )
            self.type = Error_Type(self.get_token(), error_msg)


class Multiply_Node(AST_Node):
    def __init__(self, token, term_a, term_b):
        self.token = token
        self.term_a = term_a
        self.term_b = term_b

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_term_a(self):
        return self.term_a

    def get_term_b(self):
        return self.term_b

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "multiply\n"
        tree += self.term_a.__str__(num_tabs + 1) + "\n"
        tree += self.term_b.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        self.term_a.set_type(symbol_table, current_func)
        self.term_b.set_type(symbol_table, current_func)
        if (
            type(self.term_a.get_type()) == Int_Type
            and type(self.term_b.get_type()) == Int_Type
        ):
            self.type = Int_Type()
        else:
            error_msg = (
                "    Can't compute multiplication with types:\n        "
                + str(self.term_a.get_type())
                + " and "
                + str(self.term_b.get_type())
            )
            self.type = Error_Type(self.get_token(), error_msg)


class Divide_Node(AST_Node):
    def __init__(self, token, term_a, term_b):
        self.token = token
        self.term_a = term_a
        self.term_b = term_b

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_term_a(self):
        return self.term_a

    def get_term_b(self):
        return self.term_b

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "divide\n"
        tree += self.term_a.__str__(num_tabs + 1) + "\n"
        tree += self.term_b.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        self.term_a.set_type(symbol_table, current_func)
        self.term_b.set_type(symbol_table, current_func)
        if (
            type(self.term_a.get_type()) == Int_Type
            and type(self.term_b.get_type()) == Int_Type
        ):
            self.type = Int_Type()
        else:
            error_msg = (
                "    Can't compute division with types:\n        "
                + str(self.term_a.get_type())
                + " and "
                + str(self.term_b.get_type())
            )
            self.type = Error_Type(self.get_token(), error_msg)


class Not_Node(AST_Node):
    def __init__(self, token, factor):
        self.token = token
        self.factor = factor

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_factor(self):
        return self.factor

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "not\n"
        tree += self.factor.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        self.factor.set_type(symbol_table, current_func)
        if type(self.factor.get_type()) == Boolean_Type:
            self.type = Boolean_Type()
        else:
            error_msg = "    Can't use 'not' operator on type:\n        " + str(
                self.factor.get_type()
            )
            self.type = Error_Type(self.get_token(), error_msg)


class Negative_Node(AST_Node):
    def __init__(self, token, factor):
        self.token = token
        self.factor = factor

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_factor(self):
        return self.factor

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "negative\n"
        tree += self.factor.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        self.factor.set_type(symbol_table, current_func)
        if type(self.factor.get_type()) == Int_Type:
            self.type = Int_Type()
        else:
            error_msg = "    Can't use 'negative' operator on type:\n        " + str(
                self.factor.get_type()
            )
            self.type = Error_Type(self.get_token(), error_msg)


class Function_Call_Node(AST_Node):
    def __init__(self, token, name, arg_list):
        self.token = token
        self.name = name
        self.arg_list = arg_list

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_name(self):
        return self.name

    def get_arg_list(self):
        return self.arg_list

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "func-call\n"
        tree += self.name.__str__(num_tabs + 1) + "\n"
        tree += self.arg_list.__str__(num_tabs + 1)
        return tree

    def set_type(self, symbol_table, current_func):
        current_func_entry = symbol_table.get_entry(current_func)
        if not current_func_entry.is_func_call(self.name.get_value()):
            current_func_entry.add_func_call(self.name.get_value())

        # Function doesn't exist!
        if not symbol_table.is_entry(self.name.get_value()):
            error_msg = "    Function '" + self.name.get_value() + "' doesn't exist"
            self.type = Error_Type(self.get_token(), error_msg)
            return

        this_entry = symbol_table.get_entry(self.name.get_value())
        self.type = this_entry.get_type()
        self.arg_list.set_type(symbol_table, current_func)

        if not this_entry.is_calling_func(current_func):
            this_entry.add_calling_func(current_func)

        param_types = [
            this_entry.get_params()[param] for param in this_entry.get_params().keys()
        ]

        # For some reason functions with one argument will make the argument list
        # a single identifier, so we need to take that into account.
        if type(self.get_arg_list()) == Argument_List_Node:
            # Number of arguments don't match the function definition!
            if len(self.get_arg_list().get_args()) != len(this_entry.get_params()):
                error_msg = (
                    "    Number of args in function call: '"
                    + self.name.get_value()
                    + "'\n        not equal to number of parameters in function definition"
                )
                self.type = Error_Type(self.get_token(), error_msg)
                return

            # Check if argument types align with the function definition
            for index in range(0, len(param_types)):
                arg_type = self.get_arg_list().get_args()[index].get_type()

                # Argument types don't match the definitions parameter types.
                if type(arg_type) != type(param_types[index]):
                    error_msg = (
                        "    Incorrect argument type for parameter '"
                        + list(this_entry.get_params().keys())[index]
                        + "' in function call '"
                        + self.name.get_value()
                        + "'"
                        + "\n        Expected: "
                        + str(arg_type)
                        + ", Got: "
                        + str(param_types[index])
                    )
                    self.type = Error_Type(self.get_token(), error_msg)
                    return

            return

        # For single arguments.
        for param_type in param_types:
            arg_type = self.get_arg_list().get_type()

            if type(arg_type) != type(param_type):
                error_msg = (
                    "    Incorrect argument type for parameter '"
                    + list(this_entry.get_params().keys())[0]
                    + "' in function call '"
                    + self.name.get_value()
                    + "'"
                    + "\n        Expected: "
                    + str(arg_type)
                    + ", Got: "
                    + str(param_types[0])
                )
                self.type = Error_Type(self.get_token(), error_msg)
                return


class If_Node(AST_Node):
    def __init__(self, token, if_exp, then_exp, else_exp):
        self.token = token
        self.if_exp = if_exp
        self.then_exp = then_exp
        self.else_exp = else_exp

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_if_exp(self):
        return self.if_exp

    def get_then_exp(self):
        return self.then_exp

    def get_else_exp(self):
        return self.else_exp

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "if\n"
        tree += self.if_exp.__str__(num_tabs + 1) + "\n"
        tree += "\t" * (num_tabs + 1) + "then\n"
        tree += self.then_exp.__str__(num_tabs + 2) + "\n"
        tree += "\t" * (num_tabs + 1) + "else\n"
        tree += self.else_exp.__str__(num_tabs + 2)
        return tree

    def set_type(self, symbol_table, current_func):
        self.if_exp.set_type(symbol_table, current_func)
        self.then_exp.set_type(symbol_table, current_func)
        self.else_exp.set_type(symbol_table, current_func)
        if type(self.if_exp.get_type()) == Boolean_Type:
            error_msg = ""
            if type(self.then_exp.get_type()) == Error_Type:
                error_msg += self.then_exp.get_type().get_msg()
                self.type = Error_Type(self.get_token(), error_msg)
            if type(self.else_exp.get_type()) == Error_Type:
                error_msg += self.else_exp.get_type().get_msg()
                self.type = Error_Type(self.get_token(), error_msg)
            if type(self.then_exp.get_type()) == type(self.else_exp.get_type()):
                self.type = self.then_exp.get_type()
            else:
                error_msg += (
                    "\n    Then and else types must match\n        "
                    + "Then: "
                    + str(self.then_exp.get_type())
                    + ", Else: "
                    + str(self.else_exp.get_type())
                )
                self.type = Error_Type(self.get_token(), error_msg)
        else:
            error_msg = "    If test must be of type BOOLEAN"
            if type(self.then_exp.get_type()) != type(self.else_exp.get_type()):
                error_msg += (
                    "\n    Then and else types must match\n        "
                    + "Then: "
                    + str(self.then_exp.get_type())
                    + ", Else: "
                    + str(self.else_exp.get_type())
                )
            self.type = Error_Type(self.get_token(), error_msg)


class Argument_List_Node(AST_Node):
    def __init__(self, token, args):
        self.token = token
        self.args = args

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_args(self):
        return self.args

    def __str__(self, num_tabs=0):
        tree = "\t" * num_tabs + "arg-list\n"
        if len(self.args) == 0:
            tree += "\t" * (num_tabs + 1) + "none"
        else:
            for arg in self.args:
                tree += arg.__str__(num_tabs + 1) + "\n"
        return tree[:-1]

    def set_type(self, symbol_table, current_func):
        for arg in self.args:
            arg.set_type(symbol_table, current_func)


class Boolean_Lit_Node(AST_Node):
    def __init__(self, token, value):
        self.token = token
        self.value = value

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_value(self):
        return self.value

    def __str__(self, num_tabs=0):
        return "\t" * num_tabs + "bool\t" + self.value

    def set_type(self, symbol_table, current_func):
        self.type = Boolean_Type()


class Integer_Lit_Node(AST_Node):
    def __init__(self, token, value):
        self.token = token
        self.value = value

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_value(self):
        return self.value

    def __str__(self, num_tabs=0):
        return "\t" * num_tabs + "int\t" + str(self.value)

    def set_type(self, symbol_table, current_func):
        self.type = Int_Type()


class Name_Node(AST_Node):
    def __init__(self, token, value):
        self.token = token
        self.value = value

        self.type = None_Type()

    def get_token(self):
        return self.token

    def get_value(self):
        return self.value

    def __str__(self, num_tabs=0):
        return "\t" * num_tabs + "name\t" + self.value

    def set_type(self, symbol_table, current_func):
        self.type = None_Type()
