import src.data_structs.ast_nodes as ast_nodes
import copy
from src.data_structs.symbol_table import Symbol_Table, Symbol_Table_Entry


class Optimizer:
    def __init__(self, ast: ast_nodes.Definition_List_Node, symbol_table: Symbol_Table):
        self.ast = ast
        self.symbol_table = symbol_table
        self.definition_nodes = self.ast.get_definitions()

        self.term_ops = [
            ast_nodes.Add_Node,
            ast_nodes.Subtract_Node,
            ast_nodes.Multiply_Node,
            ast_nodes.Divide_Node,
            ast_nodes.Or_Node,
            ast_nodes.And_Node,
        ]

        self.exp_ops = [ast_nodes.Equals_Node, ast_nodes.Less_Than_Node]

        self.factor_ops = [ast_nodes.Negative_Node, ast_nodes.Not_Node]

    def optimize_ast(self, optimization_mode: int):
        return self.optimize_ast_helper(optimization_mode, "")

    # Currently optimizes math expressions by pre-calculating literal expressions.
    # For example. (10 + 10) would just compile as 20.
    #
    # optimization_mode: 0 = None,
    #                    1 = Pre-calc expressions,
    #                    2 = function inlining,
    #                    3 = Both 1 and 2
    def optimize_ast_helper(
        self, optimization_mode: int, definition_name, current_node=None, prev_node=None
    ):
        if not current_node:
            self.optimize_ast_helper(optimization_mode, "", self.ast)
            return current_node

        if type(current_node) == ast_nodes.Definition_List_Node:
            for definition in current_node.get_definitions():
                self.optimize_ast_helper(
                    optimization_mode,
                    definition.get_name().get_value(),
                    definition,
                    current_node,
                )
            return current_node

        if type(current_node) == ast_nodes.Definition_Node:
            return self.optimize_ast_helper(
                optimization_mode,
                current_node.get_name().get_value(),
                current_node.get_body(),
                current_node,
            )

        if type(current_node) == ast_nodes.Body_Node:
            for expression in current_node.get_expressions():
                self.optimize_ast_helper(
                    optimization_mode, definition_name, expression, current_node
                )
            return current_node

        if type(current_node) in self.term_ops:
            left = current_node.get_term_a()
            right = current_node.get_term_b()

            # If left and right are constants we can replace nodes.
            if (
                type(left) == ast_nodes.Integer_Lit_Node
                and type(right) == ast_nodes.Integer_Lit_Node
                and optimization_mode in [1, 3]
            ):
                result = None
                if type(current_node) == ast_nodes.Add_Node:
                    result = left.get_value() + right.get_value()

                if type(current_node) == ast_nodes.Subtract_Node:
                    result = left.get_value() - right.get_value()

                if type(current_node) == ast_nodes.Multiply_Node:
                    result = left.get_value() * right.get_value()

                if type(current_node) == ast_nodes.Divide_Node:
                    result = left.get_value() // right.get_value()

                # Replace current_node with new integer node.
                new_node = ast_nodes.Integer_Lit_Node(left.get_token(), result)
                self.__replace_node(current_node, new_node, prev_node)
                # Re-run optimizer
                return self.optimize_ast_helper(optimization_mode, definition_name)

            self.optimize_ast_helper(
                optimization_mode,
                definition_name,
                current_node.get_term_a(),
                current_node,
            )
            self.optimize_ast_helper(
                optimization_mode,
                definition_name,
                current_node.get_term_b(),
                current_node,
            )
            return current_node

        # inline function optimization.
        if type(current_node) == ast_nodes.Function_Call_Node:
            if optimization_mode not in [2, 3]:
                return current_node

            # Don't touch recursive calls.
            if current_node.get_name().get_value() == definition_name:
                return current_node

            args = current_node.get_arg_list()
            if type(args) != ast_nodes.Argument_List_Node:
                args = [args]
            else:
                args = args.get_args()

            for arg in args:
                self.optimize_ast_helper(
                    optimization_mode, definition_name, arg, current_node
                )

            # Grab function call symbol table, and create a
            # mapping for replacing arguments with function call values.
            function_call_entry: Symbol_Table_Entry = self.symbol_table.get_entry(
                current_node.get_name().get_value()
            )
            # Check if this function calls itself
            if (
                current_node.get_name().get_value()
                in function_call_entry.get_func_calls()
            ):
                return current_node

            # Also don't mess with mutual recursive
            if (
                    current_node.get_name().get_value()
                    in function_call_entry.get_func_calls()
                    and definition_name in function_call_entry.get_func_calls()
            ):
                return current_node

            param_map = {}
            for index in range(0, len(function_call_entry.get_param_list())):
                param_list = function_call_entry.get_param_list()
                arg_list = args

                param_map[param_list[index]] = arg_list[index]

            # Walk the function's Body_Node expression and replace arguments with
            # mapping.
            calling_func_body = self.__inline_function(
                current_node.get_name().get_value(), param_map
            )

            # replace function call with modified expression.
            if calling_func_body:
                self.__replace_node(current_node, calling_func_body, prev_node)
                # Reset optimization.
                return self.optimize_ast_helper(optimization_mode, definition_name)

            return current_node

        if type(current_node) in self.exp_ops:
            self.optimize_ast_helper(
                optimization_mode,
                definition_name,
                current_node.get_exp_a(),
                current_node,
            )
            self.optimize_ast_helper(
                optimization_mode,
                definition_name,
                current_node.get_exp_b(),
                current_node,
            )

        if type(current_node) in self.factor_ops:
            self.optimize_ast_helper(
                optimization_mode,
                definition_name,
                current_node.get_factor(),
                current_node,
            )

        if type(current_node) == ast_nodes.If_Node:
            self.optimize_ast_helper(
                optimization_mode,
                definition_name,
                current_node.get_if_exp(),
                current_node,
            )
            self.optimize_ast_helper(
                optimization_mode,
                definition_name,
                current_node.get_then_exp(),
                current_node,
            )
            self.optimize_ast_helper(
                optimization_mode,
                definition_name,
                current_node.get_else_exp(),
                current_node,
            )

    # Replace 'current_node' with 'replace_node'.
    def __replace_node(self, current_node, replace_node, prev_node):
        if type(prev_node) == ast_nodes.Body_Node:
            prev_node.get_expressions()[0] = replace_node
            return

        # I think 'print' doesn't actually have a argument list.
        if type(prev_node) == ast_nodes.Function_Call_Node:
            if type(prev_node.get_arg_list()) == ast_nodes.Argument_List_Node:
                index = prev_node.get_arg_list().get_args().index(current_node)
                prev_node.get_arg_list().get_args()[index] = replace_node
                return
            
            prev_node.arg_list = replace_node
            return

        if type(prev_node) == ast_nodes.Argument_List_Node:
            index = prev_node.get_args().index(current_node)
            prev_node.get_args()[index] = replace_node
            return

        if type(prev_node) in self.term_ops:
            if prev_node.get_term_a() == current_node:
                prev_node.term_a = replace_node
            if prev_node.get_term_b() == current_node:
                prev_node.term_b = replace_node
            return

        if type(prev_node) in self.exp_ops:
            if prev_node.get_exp_a() == current_node:
                prev_node.exp_a = replace_node
            if prev_node.get_exp_b() == current_node:
                prev_node.exp_b = replace_node
            return

        if type(prev_node) == ast_nodes.If_Node:
            if prev_node.get_if_exp() == current_node:
                prev_node.if_exp = replace_node
            if prev_node.get_then_exp() == current_node:
                prev_node.then_exp = replace_node
            if prev_node.get_else_exp() == current_node:
                prev_node.else_exp = replace_node
            return

    def __inline_function_helper(self, current_node, prev_node, param_map):
        if type(current_node) == ast_nodes.Identifier_Node:
            self.__replace_node(
                current_node,
                param_map[current_node.get_value()],
                prev_node,
            )

        if type(current_node) == ast_nodes.Argument_List_Node:
            for arg in current_node.get_args():
                self.__inline_function_helper(arg, current_node, param_map)

        if type(current_node) == ast_nodes.Function_Call_Node:
            self.__inline_function_helper(
                current_node.get_arg_list(), current_node, param_map
            )

        if type(current_node) == ast_nodes.If_Node:
            self.__inline_function_helper(
                current_node.get_if_exp(), current_node, param_map
            )
            self.__inline_function_helper(
                current_node.get_then_exp(), current_node, param_map
            )
            self.__inline_function_helper(
                current_node.get_else_exp(), current_node, param_map
            )

        if type(current_node) in self.term_ops:
            self.__inline_function_helper(
                current_node.get_term_a(), current_node, param_map
            )
            self.__inline_function_helper(
                current_node.get_term_b(), current_node, param_map
            )

        if type(current_node) in self.exp_ops:
            self.__inline_function_helper(
                current_node.get_exp_a(), current_node, param_map
            )
            self.__inline_function_helper(
                current_node.get_exp_b(), current_node, param_map
            )

        if type(current_node) in self.factor_ops:
            self.__inline_function_helper(
                current_node.get_factor(), current_node, param_map
            )

        return True

    # Skim through AST and replace identifier nodes with calling function's
    # input values.
    def __inline_function(self, function_name: str, param_map: dict):
        body_node = None

        # Grab the called function's body.
        for definition in self.ast.get_definitions():
            if function_name == definition.get_name().get_value():
                # Create a new copy of the definition's body,
                # so the original function's body doesn't get changed.
                body_node = copy.deepcopy(definition.get_body())
                break

        if not body_node:
            return None

        # If there are more than one expression in the body. Don't in-line it.
        if len(body_node.get_expressions()) > 1:
            return None

        expression_node = body_node.get_expressions()[0]

        if type(expression_node) == ast_nodes.If_Node:
            return None

        # Replace all identifiers with the calling functions values.
        helper_status = self.__inline_function_helper(
            expression_node, body_node, param_map
        )

        if not helper_status:
            return None

        return body_node.get_expressions()[0]
