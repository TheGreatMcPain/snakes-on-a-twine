from src.data_structs.token_twine import TokenType

def parse_table_info(table: dict):
    level1_keys = list(table.keys())
    level2_keys = list(table[level1_keys[0]].keys())

    init_row_keys_string = "Row Keys: "
    row_keys_string = init_row_keys_string + str(level1_keys[0]) + "\n"

    for key in level1_keys[1:]:
        row_keys_string += " "*len(init_row_keys_string) + key + "\n"

    print(row_keys_string)
    print()

    init_col_keys_string = "Column Keys: "
    col_keys_string = init_col_keys_string + str(level2_keys[0]) + "\n"

    for key in level2_keys[1:]:
        col_keys_string += " "*len(init_col_keys_string) + str(key) + "\n"

    print(col_keys_string)
    print()

    # Print non None values
    print("Values that are not None:\n")
    for row in level1_keys:
        for col in level2_keys:
            value = table[row][col]

            if value:
                print("table[\"" + str(row) + "\"][" + str(col) + "] = " + str(value))

def gen_parse_table():
    parse_table = {}

    non_terminals = [
        "PROGRAM",
        "DEFINITION-LIST",
        "DEFINITION-LIST'",
        "DEFINITION",
        "FUNCTION-RETURN",
        "BODY",
        "PRINT-EXPRESSION",
        "PARAMETER-LIST",
        "FORMAL-PARAMETERS",
        "FORMAL-PARAMETERS'",
        "ID-WITH-TYPE",
        "TYPE",
        "EXPRESSION",
        "EXPRESSION'",
        "SIMPLE-EXPRESSION",
        "SIMPLE-EXPRESSION'",
        "TERM",
        "TERM'",
        "FACTOR",
        "ARGUMENT-LIST'",
        "ARGUMENT-LIST",
        "FORMAL-ARGUMENTS",
        "FORMAL-ARGUMENTS'",
        "LITERAL"
    ]

    terminals = [
        TokenType.op_plus,
        TokenType.op_minus,
        TokenType.op_multiply,
        TokenType.op_divide,
        TokenType.op_and,
        TokenType.op_or,
        TokenType.op_not,
        TokenType.op_equals,
        TokenType.op_lessthan,
        TokenType.punct_leftparen,
        TokenType.punct_rightparen,
        TokenType.punct_comma,
        TokenType.punct_colon,
        TokenType.int_token,
        TokenType.bool_token,
        TokenType.id_token,
        TokenType.keyword_f,
        TokenType.keyword_returns,
        TokenType.id_print,
        TokenType.typename_integer,
        TokenType.typename_boolean,
        TokenType.keyword_if,
        TokenType.keyword_else,
        TokenType.end_of_file
    ]

    for non_terminal in non_terminals:
        parse_table[non_terminal] = {}

        for terminal in terminals:
            parse_table[non_terminal][terminal] = None

    parse_table["PROGRAM"][TokenType.id_token] = 1

    parse_table["DEFINITION-LIST"][TokenType.id_token] = 2
    parse_table["DEFINITION-LIST'"][TokenType.id_token] = 3
    parse_table["DEFINITION-LIST'"][TokenType.end_of_file] = 4

    parse_table["DEFINITION"][TokenType.id_token] = 5

    parse_table["FUNCTION-RETURN"][TokenType.keyword_returns] = 6

    parse_table["BODY"][TokenType.op_minus] = 8
    parse_table["BODY"][TokenType.op_not] = 8
    parse_table["BODY"][TokenType.punct_leftparen] = 8
    parse_table["BODY"][TokenType.int_token] = 8
    parse_table["BODY"][TokenType.bool_token] = 8
    parse_table["BODY"][TokenType.id_token] = 8
    parse_table["BODY"][TokenType.id_print] = 7
    parse_table["BODY"][TokenType.keyword_if] = 8

    parse_table["PRINT-EXPRESSION"][TokenType.id_print] = 9

    parse_table["PARAMETER-LIST"][TokenType.id_token] = 11
    parse_table["PARAMETER-LIST"][TokenType.keyword_returns] = 10

    parse_table["FORMAL-PARAMETERS"][TokenType.id_token] = 12
    parse_table["FORMAL-PARAMETERS'"][TokenType.punct_comma] = 13
    parse_table["FORMAL-PARAMETERS'"][TokenType.keyword_returns] = 14

    parse_table["ID-WITH-TYPE"][TokenType.id_token] = 15

    parse_table["TYPE"][TokenType.typename_integer] = 16
    parse_table["TYPE"][TokenType.typename_boolean] = 17

    parse_table["EXPRESSION"][TokenType.op_minus] = 18
    parse_table["EXPRESSION"][TokenType.op_not] = 18
    parse_table["EXPRESSION"][TokenType.punct_leftparen] = 18
    parse_table["EXPRESSION"][TokenType.int_token] = 18
    parse_table["EXPRESSION"][TokenType.bool_token] = 18
    parse_table["EXPRESSION"][TokenType.id_token] = 18
    parse_table["EXPRESSION"][TokenType.keyword_if] = 18

    parse_table["EXPRESSION'"][TokenType.op_plus] = 21
    parse_table["EXPRESSION'"][TokenType.op_multiply] = 21
    parse_table["EXPRESSION'"][TokenType.op_divide] = 21
    parse_table["EXPRESSION'"][TokenType.op_and] = 21
    parse_table["EXPRESSION'"][TokenType.op_or] = 21
    parse_table["EXPRESSION'"][TokenType.op_equals] = 19
    parse_table["EXPRESSION'"][TokenType.op_lessthan] = 20
    parse_table["EXPRESSION'"][TokenType.punct_rightparen] = 21
    parse_table["EXPRESSION'"][TokenType.punct_comma] = 21
    parse_table["EXPRESSION'"][TokenType.id_token] = 21
    parse_table["EXPRESSION'"][TokenType.keyword_else] = 21
    parse_table["EXPRESSION'"][TokenType.end_of_file] = 21

    parse_table["SIMPLE-EXPRESSION"][TokenType.op_minus] = 22
    parse_table["SIMPLE-EXPRESSION"][TokenType.op_not] = 22
    parse_table["SIMPLE-EXPRESSION"][TokenType.punct_leftparen] = 22
    parse_table["SIMPLE-EXPRESSION"][TokenType.int_token] = 22
    parse_table["SIMPLE-EXPRESSION"][TokenType.bool_token] = 22
    parse_table["SIMPLE-EXPRESSION"][TokenType.id_token] = 22
    parse_table["SIMPLE-EXPRESSION"][TokenType.keyword_if] = 22

    parse_table["SIMPLE-EXPRESSION'"][TokenType.op_plus] = 24
    parse_table["SIMPLE-EXPRESSION'"][TokenType.op_minus] = 25
    parse_table["SIMPLE-EXPRESSION'"][TokenType.op_multiply] = 26
    parse_table["SIMPLE-EXPRESSION'"][TokenType.op_divide] = 26
    parse_table["SIMPLE-EXPRESSION'"][TokenType.op_and] = 26
    parse_table["SIMPLE-EXPRESSION'"][TokenType.op_or] = 23
    parse_table["SIMPLE-EXPRESSION'"][TokenType.op_equals] = 26
    parse_table["SIMPLE-EXPRESSION'"][TokenType.op_lessthan] = 26
    parse_table["SIMPLE-EXPRESSION'"][TokenType.punct_rightparen] = 26
    parse_table["SIMPLE-EXPRESSION'"][TokenType.punct_comma] = 26
    parse_table["SIMPLE-EXPRESSION'"][TokenType.id_token] = 26
    parse_table["SIMPLE-EXPRESSION'"][TokenType.keyword_else] = 26
    parse_table["SIMPLE-EXPRESSION'"][TokenType.end_of_file] = 26

    parse_table["TERM"][TokenType.op_minus] = 27
    parse_table["TERM"][TokenType.op_not] = 27
    parse_table["TERM"][TokenType.punct_leftparen] = 27
    parse_table["TERM"][TokenType.int_token] = 27
    parse_table["TERM"][TokenType.bool_token] = 27
    parse_table["TERM"][TokenType.id_token] = 27
    parse_table["TERM"][TokenType.keyword_if] = 27
    parse_table["TERM'"][TokenType.op_plus] = 31

    parse_table["TERM'"][TokenType.op_minus] = 31
    parse_table["TERM'"][TokenType.op_multiply] = 29
    parse_table["TERM'"][TokenType.op_divide] = 30
    parse_table["TERM'"][TokenType.op_and] = 28
    parse_table["TERM'"][TokenType.op_or] = 31
    parse_table["TERM'"][TokenType.op_equals] = 31
    parse_table["TERM'"][TokenType.op_lessthan] = 31
    parse_table["TERM'"][TokenType.punct_rightparen] = 31
    parse_table["TERM'"][TokenType.punct_comma] = 31
    parse_table["TERM'"][TokenType.id_token] = 31
    parse_table["TERM'"][TokenType.keyword_else] = 31
    parse_table["TERM'"][TokenType.end_of_file] = 31

    parse_table["FACTOR"][TokenType.op_minus] = 34
    parse_table["FACTOR"][TokenType.op_not] = 33
    parse_table["FACTOR"][TokenType.punct_leftparen] = 37
    parse_table["FACTOR"][TokenType.int_token] = 32
    parse_table["FACTOR"][TokenType.bool_token] = 32
    parse_table["FACTOR"][TokenType.id_token] = 35
    parse_table["FACTOR"][TokenType.keyword_if] = 36

    parse_table["ARGUMENT-LIST'"][TokenType.op_plus] = 39
    parse_table["ARGUMENT-LIST'"][TokenType.op_minus] = 39
    parse_table["ARGUMENT-LIST'"][TokenType.op_multiply] = 39
    parse_table["ARGUMENT-LIST'"][TokenType.op_divide] = 39
    parse_table["ARGUMENT-LIST'"][TokenType.op_and] = 39
    parse_table["ARGUMENT-LIST'"][TokenType.op_or] = 39
    parse_table["ARGUMENT-LIST'"][TokenType.op_equals] = 39
    parse_table["ARGUMENT-LIST'"][TokenType.op_lessthan] = 39
    parse_table["ARGUMENT-LIST'"][TokenType.punct_leftparen] = 38
    parse_table["ARGUMENT-LIST'"][TokenType.punct_rightparen] = 39
    parse_table["ARGUMENT-LIST'"][TokenType.punct_comma] = 39
    parse_table["ARGUMENT-LIST'"][TokenType.id_token] = 39
    parse_table["ARGUMENT-LIST'"][TokenType.keyword_else] = 39
    parse_table["ARGUMENT-LIST'"][TokenType.end_of_file] = 39

    parse_table["ARGUMENT-LIST"][TokenType.op_minus] = 41
    parse_table["ARGUMENT-LIST"][TokenType.op_not] = 41
    parse_table["ARGUMENT-LIST"][TokenType.punct_leftparen] = 41
    parse_table["ARGUMENT-LIST"][TokenType.punct_rightparen] = 40
    parse_table["ARGUMENT-LIST"][TokenType.int_token] = 41
    parse_table["ARGUMENT-LIST"][TokenType.bool_token] = 41
    parse_table["ARGUMENT-LIST"][TokenType.id_token] = 41
    parse_table["ARGUMENT-LIST"][TokenType.keyword_if] = 41

    parse_table["FORMAL-ARGUMENTS"][TokenType.op_minus] = 42
    parse_table["FORMAL-ARGUMENTS"][TokenType.op_not] = 42
    parse_table["FORMAL-ARGUMENTS"][TokenType.punct_leftparen] = 42
    parse_table["FORMAL-ARGUMENTS"][TokenType.int_token] = 42
    parse_table["FORMAL-ARGUMENTS"][TokenType.bool_token] = 42
    parse_table["FORMAL-ARGUMENTS"][TokenType.id_token] = 42
    parse_table["FORMAL-ARGUMENTS"][TokenType.keyword_if] = 42

    parse_table["FORMAL-ARGUMENTS'"][TokenType.punct_rightparen] = 44
    parse_table["FORMAL-ARGUMENTS'"][TokenType.punct_comma] = 43

    parse_table["LITERAL"][TokenType.int_token] = 46
    parse_table["LITERAL"][TokenType.bool_token] = 45

    return parse_table
