from src.data_structs.ast_nodes import *
from enum import Enum

# Stack operations
def top(stack):
    return stack[-1]


def pop(stack):
    return stack.pop()


def push(element, stack):
    stack.append(element)
    return stack


# Semantic actions
class SemanticAction(Enum):
    MakeDefList = 0
    MakeDef = 1
    MakeId = 2
    MakeParamList = 3
    MakeFuncReturn = 4
    MakeBody = 5
    MakeParam = 6
    MakeType = 7
    MakeEquals = 8
    MakeLessThan = 9
    MakeOr = 10
    MakeAdd = 11
    MakeSubtract = 12
    MakeAnd = 13
    MakeMultiply = 14
    MakeDivide = 15
    MakeNot = 16
    MakeNegative = 17
    MakeFuncCall = 18
    MakeIf = 19
    MakeArgList = 20
    MakeBool = 21
    MakeInt = 22
    MakeName = 23


def make_def_list(ast_stack):
    defs = []
    while type(top(ast_stack)) == Definition_Node:
        defs.append(pop(ast_stack))
        if len(ast_stack) == 0:
            break
    new_node = Definition_List_Node(defs[-1].get_token(), defs[::-1])
    return push(new_node, ast_stack)


def make_def(ast_stack):
    body = pop(ast_stack)
    func_return = pop(ast_stack)
    param_list = pop(ast_stack)
    id = pop(ast_stack)
    new_node = Definition_Node(id.get_token(), id, param_list, func_return, body)
    return push(new_node, ast_stack)


def make_id(token, matched_terminal, ast_stack):
    new_node = Identifier_Node(token, matched_terminal)
    return push(new_node, ast_stack)


def make_param_list(ast_stack):
    params = []
    while type(top(ast_stack)) == Param_Node:
        params.append(pop(ast_stack))
        if len(ast_stack) == 0:
            break
    token = None
    if len(params) > 0:
        token = params[-1].get_token()
    new_node = Parameter_List_Node(token, params[::-1])
    return push(new_node, ast_stack)


def make_func_return(ast_stack):
    type = pop(ast_stack)
    new_node = Function_Return_Node(type.get_token(), type)
    return push(new_node, ast_stack)


def make_body(ast_stack):
    expressions = []
    while is_expression(top(ast_stack)):
        expressions.append(pop(ast_stack))
        if len(ast_stack) == 0:
            break
    new_node = Body_Node(expressions[-1].get_token(), expressions[::-1])
    return push(new_node, ast_stack)


def make_param(ast_stack):
    type = pop(ast_stack)
    id = pop(ast_stack)
    new_node = Param_Node(id.get_token(), id, type)
    return push(new_node, ast_stack)


def make_type(token, matched_terminal, ast_stack):
    new_node = Type_Node(token, matched_terminal)
    return push(new_node, ast_stack)


def make_equals(ast_stack):
    exp_b = pop(ast_stack)
    exp_a = pop(ast_stack)
    new_node = Equals_Node(exp_a.get_token(), exp_a, exp_b)
    return push(new_node, ast_stack)


def make_less_than(ast_stack):
    exp_b = pop(ast_stack)
    exp_a = pop(ast_stack)
    new_node = Less_Than_Node(exp_a.get_token(), exp_a, exp_b)
    return push(new_node, ast_stack)


def make_or(ast_stack):
    term_b = pop(ast_stack)
    term_a = pop(ast_stack)
    new_node = Or_Node(term_a.get_token(), term_a, term_b)
    return push(new_node, ast_stack)


# Make sure new math nodes are ordered in a way that respects
# the order of operations while also respecting parenthesis placement.
def fix_order_of_ops(ast_node, priority):
    math_ops = [Add_Node, Subtract_Node, Divide_Node, Multiply_Node]
    if not type(ast_node) in math_ops:
        return ast_node

    if not type(ast_node.get_term_b()) in math_ops:
        return ast_node

    if ast_node.get_term_b() not in priority:
        return ast_node

    # Make sure not to lose that we want to return.
    root_node = ast_node.get_term_b()

    # Find the trailing node.
    swap_node = ast_node.get_term_b()
    prev_node = None
    while (
        type(swap_node.get_term_a()) in math_ops and swap_node.get_term_a() in priority
    ):
        prev_node = swap_node
        swap_node = swap_node.get_term_a()

    # Don't override nodes that take priority.
    if swap_node not in priority:
        swap_node = prev_node

    # Append ast_node to the right side of swap_node
    ast_node.term_b = swap_node.get_term_a()
    swap_node.term_a = ast_node

    # Return new node
    return root_node


# Tracks math node groups based on parenthesis depth.
mul_div_priority = {}
add_sub_priority = {}


def make_add(ast_stack, parens):
    # Make sure the priority list is created for
    # the current parenthesis group.
    if parens not in mul_div_priority:
        mul_div_priority[parens] = []
    if parens not in add_sub_priority:
        add_sub_priority[parens] = []

    term_b = pop(ast_stack)
    term_a = pop(ast_stack)
    new_node = Add_Node(term_a.get_token(), term_a, term_b)

    # Add this node to this parenthesis group.
    # Also, add to "multiply" and "divide" group, because
    # this will allow "multiply" and "divide" to override add and subtract nodes.
    add_sub_priority[parens].append(new_node)
    mul_div_priority[parens].append(new_node)

    # Fix order of operations
    new_node = fix_order_of_ops(new_node, add_sub_priority[parens])

    return push(new_node, ast_stack)


def make_subtract(ast_stack, parens):
    # Make sure the priority list is created for
    # the current parenthesis group.
    if parens not in mul_div_priority:
        mul_div_priority[parens] = []
    if parens not in add_sub_priority:
        add_sub_priority[parens] = []

    term_b = pop(ast_stack)
    term_a = pop(ast_stack)
    new_node = Subtract_Node(term_a.get_token(), term_a, term_b)

    # Add this node to this parenthesis group.
    # Also, add to "multiply" and "divide" group, because
    # this will allow "multiply" and "divide" to override add and subtract nodes.
    add_sub_priority[parens].append(new_node)
    mul_div_priority[parens].append(new_node)

    # Fix order of operations
    new_node = fix_order_of_ops(new_node, add_sub_priority[parens])

    return push(new_node, ast_stack)


def make_and(ast_stack):
    term_b = pop(ast_stack)
    term_a = pop(ast_stack)
    new_node = And_Node(term_a.get_token(), term_a, term_b)
    return push(new_node, ast_stack)


def make_multiply(ast_stack, parens):
    # Make sure the priority list is created for
    # the current parenthesis group.
    if parens not in mul_div_priority.keys():
        mul_div_priority[parens] = []
    if parens not in add_sub_priority.keys():
        add_sub_priority[parens] = []

    term_b = pop(ast_stack)
    term_a = pop(ast_stack)
    new_node = Multiply_Node(term_a.get_token(), term_a, term_b)

    # Add this node to this parenthesis group.
    # Also, exclude from add and subtract group.
    # This will prevent add and subtract from taking priority.
    mul_div_priority[parens].append(new_node)

    # Fix order of operations
    new_node = fix_order_of_ops(new_node, mul_div_priority[parens])

    return push(new_node, ast_stack)


def make_divide(ast_stack, parens):
    # Make sure the priority list is created for
    # the current parenthesis group.
    if parens not in mul_div_priority:
        mul_div_priority[parens] = []
    if parens not in add_sub_priority:
        add_sub_priority[parens] = []

    term_b = pop(ast_stack)
    term_a = pop(ast_stack)
    new_node = Divide_Node(term_a.get_token(), term_a, term_b)

    # Add this node to this parenthesis group.
    # Also, exclude from add and subtract group.
    # This will prevent add and subtract from taking priority.
    mul_div_priority[parens].append(new_node)

    # Fix order of operations
    new_node = fix_order_of_ops(new_node, mul_div_priority[parens])

    return push(new_node, ast_stack)


def make_not(ast_stack):
    factor = pop(ast_stack)
    new_node = Not_Node(factor.get_token(), factor)
    return push(new_node, ast_stack)


def make_negative(ast_stack):
    factor = pop(ast_stack)
    new_node = Negative_Node(factor.get_token(), factor)
    return push(new_node, ast_stack)


def make_func_call(ast_stack):
    arg_list = pop(ast_stack)
    id = pop(ast_stack)
    new_node = Function_Call_Node(id.get_token(), id, arg_list)
    return push(new_node, ast_stack)


def make_if(ast_stack):
    else_exp = pop(ast_stack)
    then_exp = pop(ast_stack)
    if_exp = pop(ast_stack)
    new_node = If_Node(if_exp.get_token(), if_exp, then_exp, else_exp)
    return push(new_node, ast_stack)


def make_arg_list(ast_stack):
    args = []
    while is_expression(top(ast_stack)):
        args.append(pop(ast_stack))
        if len(ast_stack) == 0:
            break
    token = None
    arg_list = []
    if len(args) > 0:
        token = args[-1].get_token()
        arg_list = args[::-1]
    new_node = Argument_List_Node(token, arg_list)
    return push(new_node, ast_stack)


def make_bool(token, matched_terminal, ast_stack):
    new_node = Boolean_Lit_Node(token, matched_terminal)
    return push(new_node, ast_stack)


def make_int(token, matched_terminal, ast_stack):
    new_node = Integer_Lit_Node(token, matched_terminal)
    return push(new_node, ast_stack)


def make_name(token, matched_terminal, ast_stack):
    new_node = Name_Node(token, matched_terminal)
    return push(new_node, ast_stack)


# Helper methods
def is_expression(node):
    return (
        type(node) == Function_Call_Node
        or type(node) == Equals_Node
        or type(node) == Less_Than_Node
        or type(node) == Or_Node
        or type(node) == Add_Node
        or type(node) == Subtract_Node
        or type(node) == And_Node
        or type(node) == Multiply_Node
        or type(node) == Divide_Node
        or type(node) == Boolean_Lit_Node
        or type(node) == Integer_Lit_Node
        or type(node) == Not_Node
        or type(node) == Negative_Node
        or type(node) == If_Node
        or type(node) == Identifier_Node
    )


# Table for matching actions to their FUNCTION
action_table = {
    SemanticAction.MakeDefList: make_def_list,
    SemanticAction.MakeDef: make_def,
    SemanticAction.MakeId: make_id,
    SemanticAction.MakeParamList: make_param_list,
    SemanticAction.MakeFuncReturn: make_func_return,
    SemanticAction.MakeBody: make_body,
    SemanticAction.MakeParam: make_param,
    SemanticAction.MakeType: make_type,
    SemanticAction.MakeEquals: make_equals,
    SemanticAction.MakeLessThan: make_less_than,
    SemanticAction.MakeOr: make_or,
    SemanticAction.MakeAdd: make_add,
    SemanticAction.MakeSubtract: make_subtract,
    SemanticAction.MakeAnd: make_and,
    SemanticAction.MakeMultiply: make_multiply,
    SemanticAction.MakeDivide: make_divide,
    SemanticAction.MakeNot: make_not,
    SemanticAction.MakeNegative: make_negative,
    SemanticAction.MakeFuncCall: make_func_call,
    SemanticAction.MakeIf: make_if,
    SemanticAction.MakeArgList: make_arg_list,
    SemanticAction.MakeBool: make_bool,
    SemanticAction.MakeInt: make_int,
    SemanticAction.MakeName: make_name,
}
