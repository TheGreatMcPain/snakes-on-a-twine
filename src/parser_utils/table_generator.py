from src.parser_utils.grammer_new import grammer_dict
from src.parser_utils.semantic_actions import SemanticAction
from src.data_structs.token_twine import TokenType

# Generate first set for a non-terminal
def get_first_set(non_terminal: str, productions: list = None):

    first_set = []

    if not productions:
        productions = grammer_dict[non_terminal]

    for production in productions:
        first_value = production[0]

        if first_value not in grammer_dict.keys():
            if type(first_value) == SemanticAction:
                first_set.append("ε")
            else:
                first_set.append(first_value)
        else:
            first_set += get_first_set(first_value)

    return first_set


followSetsCache = {}


def get_follow_set(non_terminal: str):
    # Follow rules
    #
    # 1. First put "$" in Follow(S) (S is the start symbol)
    # 2. If A -> aBb then everything in FIRST(b) except for "ε" in FOLLOW(B)
    # 3. If A -> aB then everything in FOLLOW(A) is in FOLLOW(B)
    # 4. If A -> aBb where FIRST(b) contains "ε"
    #    then everything in FOLLOW(A) is in FOLLOW(B)

    # Rule 1
    if non_terminal == "PROGRAM":
        followSetsCache["PROGRAM"] = [TokenType.end_of_file]
        return followSetsCache["PROGRAM"]

    followSetsCache[non_terminal] = []

    for key in grammer_dict.keys():
        for production in grammer_dict[key]:
            production = [x for x in production if type(x) != SemanticAction]

            for item_index in range(0, len(production)):
                # Only process the input non_terminal
                if production[item_index] != non_terminal:
                    continue

                # Rule 3
                if item_index + 1 == len(production):
                    if key in followSetsCache.keys():
                        followSetsCache[non_terminal] += followSetsCache[key]
                    else:
                        followSetsCache[non_terminal] += get_follow_set(key)
                    continue

                # Rule 2
                if production[item_index + 1] in grammer_dict.keys():
                    next_first_set = get_first_set(production[item_index + 1])

                    # Rule 4
                    if "ε" in next_first_set:
                        next_first_set.remove("ε")
                        next_first_set += get_follow_set(key)

                    followSetsCache[non_terminal] += next_first_set
                else:
                    followSetsCache[non_terminal].append(production[item_index + 1])

    # Get rid of any dups
    new_set = []
    for item in followSetsCache[non_terminal]:
        if item not in new_set:
            new_set.append(item)
    followSetsCache[non_terminal] = new_set

    return followSetsCache[non_terminal]


def gen_parse_table():
    parse_table = {}
    first_sets = {}
    follow_sets = {}

    non_terminals = [
        "PROGRAM",
        "DEFINITION-LIST",
        "DEFINITION-LIST'",
        "DEFINITION",
        "FUNCTION-RETURN",
        "BODY",
        "PRINT-EXPRESSION",
        "PARAMETER-LIST",
        "FORMAL-PARAMETERS",
        "FORMAL-PARAMETERS'",
        "ID-WITH-TYPE",
        "TYPE",
        "EXPRESSION",
        "EXPRESSION'",
        "SIMPLE-EXPRESSION",
        "SIMPLE-EXPRESSION'",
        "TERM",
        "TERM'",
        "FACTOR",
        "ARGUMENT-LIST'",
        "ARGUMENT-LIST",
        "FORMAL-ARGUMENTS",
        "FORMAL-ARGUMENTS'",
        "LITERAL",
    ]

    terminals = [
        TokenType.op_plus,
        TokenType.op_minus,
        TokenType.op_multiply,
        TokenType.op_divide,
        TokenType.op_and,
        TokenType.op_or,
        TokenType.op_not,
        TokenType.op_equals,
        TokenType.op_lessthan,
        TokenType.punct_leftparen,
        TokenType.punct_rightparen,
        TokenType.punct_comma,
        TokenType.punct_colon,
        TokenType.int_token,
        TokenType.bool_token,
        TokenType.id_token,
        TokenType.keyword_f,
        TokenType.keyword_returns,
        TokenType.id_print,
        TokenType.typename_integer,
        TokenType.typename_boolean,
        TokenType.keyword_if,
        TokenType.keyword_else,
        TokenType.end_of_file,
    ]

    # Initialize table and First/Follow sets.
    for non_term in non_terminals:
        first_sets[non_term] = get_first_set(non_term)
        follow_sets[non_term] = get_follow_set(non_term)
        parse_table[non_term] = {}

        for term in terminals:
            parse_table[non_term][term] = None

        # Get productions and map them onto the table.
        for production in grammer_dict[non_term]:
            # If the production is null use follow set
            if ["ε"] == [x for x in production if type(x) != SemanticAction]:
                for item in follow_sets[non_term]:
                    if not parse_table[non_term][item]:
                        parse_table[non_term][item] = production
            # If the production is not null use first set
            else:
                for item in get_first_set(non_term, [production]):
                    if item != "ε":
                        parse_table[non_term][item] = production

    return parse_table


if __name__ == "__main__":
    print("First sets")
    for non_terminal in grammer_dict.keys():
        print("  {}-|".format(non_terminal))
        first_set = get_first_set(non_terminal)
        for value in first_set:
            print("    {}".format(value))
    print()

    print("Follow sets")
    for non_terminal in grammer_dict.keys():
        print("  {}-|".format(non_terminal))
        for value in get_follow_set(non_terminal):
            print("    {}".format(value))
    print()

    print("Parse Table")
    parse_table = gen_parse_table()
    for non_terminal in parse_table.keys():
        for terminal in parse_table[non_terminal].keys():
            if parse_table[non_terminal][terminal]:
                print(
                    "[{}][{}] = {}".format(
                        non_terminal, terminal, parse_table[non_terminal][terminal]
                    )
                )
