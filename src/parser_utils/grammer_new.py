from src.data_structs.token_twine import TokenType
from src.parser_utils.semantic_actions import SemanticAction


grammer_dict = {
    "PROGRAM": [["DEFINITION-LIST", SemanticAction.MakeDefList]],
    "DEFINITION-LIST": [["DEFINITION", "DEFINITION-LIST'"]],
    "DEFINITION-LIST'": [["DEFINITION-LIST"], ["ε"]],
    "DEFINITION": [
        [
            TokenType.id_token,
            SemanticAction.MakeName,
            TokenType.op_equals,
            TokenType.keyword_f,
            TokenType.punct_leftparen,
            "PARAMETER-LIST",
            SemanticAction.MakeParamList,
            "FUNCTION-RETURN",
            TokenType.punct_rightparen,
            "BODY",
            SemanticAction.MakeBody,
            SemanticAction.MakeDef,
        ]
    ],
    "FUNCTION-RETURN": [
        [
            TokenType.keyword_returns,
            "TYPE",
            SemanticAction.MakeType,
            SemanticAction.MakeFuncReturn,
        ]
    ],
    "BODY": [["PRINT-EXPRESSION", "BODY"], ["EXPRESSION"]],
    "PRINT-EXPRESSION": [
        [
            TokenType.id_print,
            SemanticAction.MakeName,
            TokenType.punct_leftparen,
            "EXPRESSION",
            TokenType.punct_rightparen,
            SemanticAction.MakeFuncCall,
        ]
    ],
    "PARAMETER-LIST": [["ε"], ["FORMAL-PARAMETERS"]],
    "FORMAL-PARAMETERS": [["ID-WITH-TYPE", "FORMAL-PARAMETERS'"]],
    "FORMAL-PARAMETERS'": [[TokenType.punct_comma, "FORMAL-PARAMETERS"], ["ε"]],
    "ID-WITH-TYPE": [
        [
            TokenType.id_token,
            SemanticAction.MakeId,
            TokenType.punct_colon,
            "TYPE",
            SemanticAction.MakeType,
            SemanticAction.MakeParam,
        ]
    ],
    "TYPE": [[TokenType.typename_integer], [TokenType.typename_boolean]],
    "EXPRESSION": [["SIMPLE-EXPRESSION", "EXPRESSION'"]],
    "EXPRESSION'": [
        [TokenType.op_equals, "EXPRESSION", SemanticAction.MakeEquals],
        [TokenType.op_lessthan, "EXPRESSION", SemanticAction.MakeLessThan],
        ["ε"],
    ],
    "SIMPLE-EXPRESSION": [["TERM", "SIMPLE-EXPRESSION'"]],
    "SIMPLE-EXPRESSION'": [
        [TokenType.op_or, "SIMPLE-EXPRESSION", SemanticAction.MakeOr],
        [TokenType.op_plus, "SIMPLE-EXPRESSION", SemanticAction.MakeAdd],
        [TokenType.op_minus, "SIMPLE-EXPRESSION", SemanticAction.MakeSubtract],
        ["ε"],
    ],
    "TERM": [["FACTOR", "TERM'"]],
    "TERM'": [
        [TokenType.op_and, "TERM", SemanticAction.MakeAnd],
        [TokenType.op_multiply, "TERM", SemanticAction.MakeMultiply],
        [TokenType.op_divide, "TERM", SemanticAction.MakeDivide],
        ["ε"],
    ],
    "FACTOR": [
        ["LITERAL"],
        [TokenType.op_not, "FACTOR", SemanticAction.MakeNot],
        [TokenType.op_minus, "FACTOR", SemanticAction.MakeNegative],
        [TokenType.id_token, "ARGUMENT-LIST'"],
        [
            TokenType.keyword_if,
            TokenType.punct_leftparen,
            "EXPRESSION",
            TokenType.punct_rightparen,
            "EXPRESSION",
            TokenType.keyword_else,
            "EXPRESSION",
            SemanticAction.MakeIf,
        ],
        [TokenType.punct_leftparen, "EXPRESSION", TokenType.punct_rightparen],
    ],
    "ARGUMENT-LIST'": [
        [
            TokenType.punct_leftparen,
            SemanticAction.MakeName,
            "ARGUMENT-LIST",
            TokenType.punct_rightparen,
            SemanticAction.MakeFuncCall,
        ],
        ["ε", SemanticAction.MakeId],
    ],
    "ARGUMENT-LIST": [
        [SemanticAction.MakeArgList],
        ["FORMAL-ARGUMENTS", SemanticAction.MakeArgList],
    ],
    "FORMAL-ARGUMENTS": [["EXPRESSION", "FORMAL-ARGUMENTS'"]],
    "FORMAL-ARGUMENTS'": [[TokenType.punct_comma, "FORMAL-ARGUMENTS"], ["ε"]],
    "LITERAL": [
        [TokenType.bool_token, SemanticAction.MakeBool],
        [TokenType.int_token, SemanticAction.MakeInt],
    ],
}
