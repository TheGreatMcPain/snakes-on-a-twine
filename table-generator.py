from src.parser_utils.table_generator import get_first_set
from src.parser_utils.table_generator import get_follow_set
from src.parser_utils.grammer_new import grammer_dict
from src.parser_utils.table_generator import gen_parse_table


if __name__ == "__main__":
    print("First sets")
    for non_terminal in grammer_dict.keys():
        print("  {}-|".format(non_terminal))
        first_set = get_first_set(non_terminal)
        for value in first_set:
            print("    {}".format(value))
    print()

    print("Follow sets")
    for non_terminal in grammer_dict.keys():
        print("  {}-|".format(non_terminal))
        for value in get_follow_set(non_terminal):
            print("    {}".format(value))
    print()

    print("Parse Table")
    parse_table = gen_parse_table()
    for non_terminal in parse_table.keys():
        for terminal in parse_table[non_terminal].keys():
            if parse_table[non_terminal][terminal]:
                print(
                    "[{}][{}] = {}".format(
                        non_terminal, terminal, parse_table[non_terminal][terminal]
                    )
                )
