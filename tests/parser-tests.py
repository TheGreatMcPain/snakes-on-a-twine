# Fix path so we can access the parser

import sys
import os.path

this_file_parent_path = os.path.abspath(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
)
sys.path.append(this_file_parent_path)


import unittest
from src.parser.parser import parse
from src.twine_errors import TwineSyntacticError


# Test cases

class TestParser(unittest.TestCase):


    def test_basic_prgm(self):
        test_prgm = '''main = f(returns integer)
                            print(1)
                            1'''

        self.assertTrue(parse(test_prgm))


    def test_wrong_int(self):
        test_prgm = '''main = f(returns int)
                            1'''

        with self.assertRaises(TwineSyntacticError):
            parse(test_prgm)


    def test_uneven_parentheses(self):
        test_prgm = '''main = f(n : integer, m : integer returns integer)
                            if ((n = 10) ^ (m = 2)
                                n
                            else
                                1'''

        with self.assertRaises(TwineSyntacticError):
            parse(test_prgm)


    def test_no_else(self):
        test_prgm = '''main = f(returns integer)
                            if (10 = 10)
                                10'''

        with self.assertRaises(TwineSyntacticError):
            parse(test_prgm)


    def test_missing_f(self):
        test_prgm = '''main = (returns integer)
                            10'''

        with self.assertRaises(TwineSyntacticError):
            parse(test_prgm)


    def test_no_return(self):
        test_prgm = '''main = f(n : integer, m : boolean)
                            print(m)
                            n + 2'''

        with self.assertRaises(TwineSyntacticError):
            parse(test_prgm)


    def test_invalid_assignment(self):
        test_prgm = '''main = f(n : boolean, m : boolean returns boolean)
                            n = true
                            n ^ m'''

        with self.assertRaises(TwineSyntacticError):
            parse(test_prgm)


    def test_empty_program(self):
        test_prgm = ''

        with self.assertRaises(TwineSyntacticError):
            parse(test_prgm)


    def test_excessive_whitespace(self):
        test_prgm = '''main =
                        f(returns integer)
                        10

                        +
                        2'''

        self.assertTrue(parse(test_prgm))


# Run test cases

if __name__ == "__main__":
    unittest.main()
