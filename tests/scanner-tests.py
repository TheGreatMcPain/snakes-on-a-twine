# Fix path so we can access the scanner

import sys
import os.path

this_file_parent_path = os.path.abspath(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
)
sys.path.append(this_file_parent_path)


import unittest
from src.scanner import Scanner
from src.twine_errors import TwineLexicalError


# Test cases

class TestScanner(unittest.TestCase):


    def test_basic_prgm(self):
        test_prgm = '''main = f(returns integer)
                            print(1)
                            1'''

        scanner_obj = Scanner(program = test_prgm)

        tokens = scanner_obj.scan()
    
        for i in range(len(tokens)):
            tokens[i] = str(tokens[i])

        expected_tokens = ["identifier = main",
                                        "operator equals",
                                        "keyword = f",
                                        "punctuation leftparen",
                                        "keyword = returns",
                                        "typename = integer",
                                        "punctuation rightparen",
                                        "identifier = print",
                                        "punctuation leftparen",
                                        "integer = 1",
                                        "punctuation rightparen",
                                        "integer = 1",
                                        "end_of_file"]

        self.assertEqual(tokens, expected_tokens)


    def test_prgm_with_comments(self):
        test_prgm = '''main = f(returns integer)
                            % This is a comment
                            % with two lines
                            print(1)
                            1'''

        scanner_obj = Scanner(program = test_prgm)

        tokens = scanner_obj.scan()


        for i in range(len(tokens)):
            tokens[i] = str(tokens[i])

        expected_tokens = ["identifier = main",
                                        "operator equals",
                                        "keyword = f",
                                        "punctuation leftparen",
                                        "keyword = returns",
                                        "typename = integer",
                                        "punctuation rightparen",
                                        "identifier = print",
                                        "punctuation leftparen",
                                        "integer = 1",
                                        "punctuation rightparen",
                                        "integer = 1",
                                        "end_of_file"]

        self.assertEqual(tokens, expected_tokens)


    def test_big_int(self):
        test_prgm = '''2147483648'''

        scanner_obj = Scanner(program = test_prgm)
        
        with self.assertRaises(TwineLexicalError):
            scanner_obj.scan()


    def test_almost_big_int(self):
        test_prgm = '''2147483647'''

        scanner_obj = Scanner(program = test_prgm)

        try:
            scanner_obj.scan()
        except TwineLexicalError:
            self.fail("Error raised unexpectedly")


    def test_big_id(self):
        test_prgm = '''aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'''

        scanner_obj = Scanner(program = test_prgm)

        with self.assertRaises(TwineLexicalError):
            scanner_obj.scan()


    def test_almost_big_id(self):
        test_prgm = '''aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'''

        scanner_obj = Scanner(program = test_prgm)

        try:
            scanner_obj.scan()
        except TwineLexicalError:
            self.fail("Error raised unexpectedly")


    def test_leading_zero(self):
        test_prgm = '''01234'''

        scanner_obj = Scanner(program = test_prgm)

        with self.assertRaises(TwineLexicalError):
            scanner_obj.scan()


    def test_leading_zero_with_chars(self):
        test_prgm = '''0a'''

        scanner_obj = Scanner(program = test_prgm)

        with self.assertRaises(TwineLexicalError):
            scanner_obj.scan()


    def test_integer_with_chars(self):
        test_prgm = '''1234a5321'''

        scanner_obj = Scanner(program = test_prgm)

        with self.assertRaises(TwineLexicalError):
            scanner_obj.scan()


    def test_bad_id_start(self):
        test_prgm = '''_abc = f(returns integer)'''

        scanner_obj = Scanner(program = test_prgm)

        with self.assertRaises(TwineLexicalError):
            scanner_obj.scan()


    def test_bad_id_middle(self):
        test_prgm = '''ab@c = f(returns integer)'''

        scanner_obj = Scanner(program = test_prgm)

        with self.assertRaises(TwineLexicalError):
            scanner_obj.scan()


    def test_ok_id_start(self):
        test_prgm = '''$abc = f(returns integer)'''

        scanner_obj = Scanner(program = test_prgm)

        tokens = scanner_obj.scan()

        for i in range(len(tokens)):
            tokens[i] = str(tokens[i])

        expected_tokens = ["identifier = $abc",
                            "operator equals",
                            "keyword = f",
                            "punctuation leftparen",
                            "keyword = returns",
                            "typename = integer",
                            "punctuation rightparen",
                            "end_of_file"]

        self.assertEqual(tokens, expected_tokens)





# Run test cases

if __name__ == "__main__":
    unittest.main()
